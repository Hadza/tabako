package tabako.SupplyManager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SupplyManagerFormGUI extends Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		launch(args);
	}

	@Override
	public void start(Stage SMstage) throws Exception {
		// TODO Auto-generated method stub
		Parent root = FXMLLoader.load(getClass().getResource("SupplyForm.fxml"));
		SMstage.setTitle("Supply Manager");
		SMstage.setScene(new Scene(root));
		SMstage.show();
	}

}
