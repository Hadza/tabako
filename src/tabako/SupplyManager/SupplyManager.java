package tabako.SupplyManager;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tabako.Models.Table;

public class SupplyManager {

	public ObservableList<SupplyItem> m_items;

	public SupplyManager() {
		m_items = FXCollections.observableArrayList();
	}

	public ObservableList<SupplyItem> getObservableList() {
		return m_items;
	}

	public void addItem(String p_name, int p_amount) {
		boolean flag = false;
		for (int i = 0; i < m_items.size(); i++)
			if (m_items.get(i).getName().equals(p_name)) {
				// poruka, ta stavka vec postoji.
				flag = true;
				break;
			}
		if (flag != true)
			m_items.add(new SupplyItem(p_name, p_amount));

	}

	public void setAmount(int index, int p_amount) {
		m_items.get(index).setAmount(p_amount);
		if (p_amount > 10)
			m_items.get(index).notify = "";
	}

	public void decAmountByTable(Table t) {
		int tmp; // prosledjuje sto, a onda se iz Supply oduzimaju stavke koje
					// su na stolu
		for (int i = 0; i < t.getItems().size(); i++) // NAPOMENA: Stavke u
														// magacinu nazivati
														// identicno kao sto se
														// zovu i u meniju.
			for (int k = 0; k < m_items.size(); k++) { // NAPOMENA2: Ne mora sve
														// sto postoji u meniju
														// da se evidentira u
														// magacinu, jer se neke
														// stavke nece moci
														// kvantifikovati u
														// komadima, tipa kafa,
														// pica iz flasa...
				if (t.getItems().get(i).getName() == m_items.get(k).getName()) {
					tmp = m_items.get(k).getAmount() - t.getAmountOfItem(t.getItems().get(i));
					if (tmp > 0)
						m_items.get(k).setAmount(tmp);
					if (tmp < 10 && tmp > 0)
						m_items.get(k).notify = "You have less than 10 articles left";
				}
			}
	}

	public void removeItemByIndex(int index) {
		m_items.remove(index);

	}

	public void removeItemByObject(SupplyItem it) {
		m_items.remove(it);
	}
}
