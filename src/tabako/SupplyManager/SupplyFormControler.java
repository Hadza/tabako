package tabako.SupplyManager;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;

public class SupplyFormControler implements Initializable {
	public SupplyManager sm = new SupplyManager();
	@FXML
	public Button btn_Add, btn_Remove, btn_Close;
	@FXML
	public TextField tf_Name, tf_Amount;
	@FXML
	public TableView<SupplyItem> tv_Table;
	@FXML
	public TableColumn<SupplyItem, String> nameCol;
	@FXML
	public TableColumn<SupplyItem, String> amountCol;
	@FXML
	public VBox editPane;
	@FXML
	public MenuItem edit;
	@FXML
	public SplitPane splitPane;

	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		nameCol.setCellValueFactory(new PropertyValueFactory<SupplyItem, String>("name"));
		amountCol.setCellValueFactory(new PropertyValueFactory<SupplyItem, String>("amount"));

	}

	public void buttonCloseClicked() {
		editPane.setVisible(false);
		splitPane.setDividerPosition(0, 0);
	}

	public void buttonAddClicked() {
		if (tf_Name.getText().equals("") || tf_Amount.getText().equals("")) {
			Alert alert = new Alert(AlertType.ERROR, "Please enter name and amount ", ButtonType.OK);
			alert.showAndWait();

			if (alert.getResult() == ButtonType.OK) {
				alert.close();
			}
		} else {
			sm.addItem(tf_Name.getText(), Integer.parseInt(tf_Amount.getText()));
			tv_Table.setItems(sm.m_items);

		}
	}

	public void buttonRemoveClicked() {
		sm.removeItemByObject(tv_Table.getSelectionModel().getSelectedItem());
	}

	public void editClicked() {
		editPane.setVisible(true);
		splitPane.setDividerPosition(0, 0.298);
	}

}
