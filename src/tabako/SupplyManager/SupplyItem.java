package tabako.SupplyManager;

public class SupplyItem {
	public String notify;
	public String name;
	public int amount;

	public SupplyItem(String p_name, int p_amount) {
		name = p_name;
		amount = p_amount;
		notify = "";

	}

	public void setAmount(int p_amount) {
		amount = p_amount;
	}

	public String getName() {
		return name;
	}

	public int getAmount() {
		return amount;
	}

};
