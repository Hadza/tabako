package tabako.DBUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import tabako.BL.CaffeBL;
import tabako.Models.Item;
import tabako.Models.ItemType;
import tabako.Models.Table;
import tabako.Models.TableHistory;
import tabako.Models.User;

public class DB {
	// Fields:
	public static String DB_NAME = "caffe.db";
	private static DB instance;

	// private Constructor
	private DB() {
	}

	public static DB getInstance() {
		if (instance == null) {
			instance = new DB();
		}
		return instance;
	}

	// Main for creating Demo Data
	public static void main(String args[]) {
		List<String> locationsOfSqls = new ArrayList<String>();
		try {
			locationsOfSqls.add(readFile("database/scripts/create_db.sql"));
			locationsOfSqls.add(readFile("database/scripts/insert_demo_items.sql"));

			executeListOFUpdateStatements(locationsOfSqls);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Public methods:

	public boolean isUserAuthentified(String macAddress) {
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM DEVICE;");

			while (rs.next()) {
				if (rs.getString("MAC").equals(macAddress)) {
					return true;
				}
			}
			rs.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	private User getUser(String query) {
		User user = null;
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			rs = stmt.executeQuery(query);
			if (rs.next()) {
				user = new User(rs.getString("username"), rs.getString("password"), rs.getBoolean("isAdmin"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return user;
	}

	public User getUserByUsernameAndPassword(String username, String password) {
		return getUser("SELECT * FROM USER WHERE username='" + username + "' AND " + "PASSWORD='" + password + "'");
	}

	public User getUserByPassword(String password) {
		return getUser("SELECT * FROM USER WHERE PASSWORD='" + password + "'");
	}

	public List<User> getAllUsers(boolean includeAdmins) {
		List<User> users = new ArrayList<User>();
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			String query = "SELECT * FROM USER ";

			if (includeAdmins) {
				query += " ;";
			} else {
				query += " WHERE ISADMIN=0;";
			}

			stmt = c.createStatement();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				User user = new User(rs.getString("username"), rs.getString("password"), rs.getBoolean("isAdmin"));
				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return users;
	}

	public List<Item> getItems() {
		List<Item> items = new ArrayList<Item>();
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM STAVKA ORDER BY ID;");

			while (rs.next()) {
				Item item = new Item(rs.getInt("id"), rs.getInt("type"), rs.getString("name"), rs.getString("code"),
						rs.getLong("price"));
				items.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return items;
	}

	public List<ItemType> getItemTypes() {
		List<ItemType> itemTypes = new ArrayList<ItemType>();
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM TIP_STAVKE ORDER BY ID;");

			while (rs.next()) {
				ItemType itemType = new ItemType(rs.getInt("id"), rs.getString("name"));
				itemTypes.add(itemType);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return itemTypes;
	}

	public List<TableHistory> getTablesHistory(String username, long t1, long t2) {
		List<TableHistory> tablesHistory = new ArrayList<TableHistory>();
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String rsString = "SELECT * FROM RACUN WHERE ORDER_TIME>=" + t1 + " AND ORDER_TIME<=" + t2 + " ";

			if (username != "") {
				rsString += " AND WAITER='" + username + "'";
			}

			rsString += ";";

			rs = stmt.executeQuery(rsString);

			while (rs.next()) {

				long timestamp = rs.getLong("ORDER_TIME");
				String name = rs.getString("NAME");
				String itemsInfos = rs.getString("ITEMS_INFO");
				int sumOfItems = rs.getInt("SUM_OF_ITEMS");
				String waiterUsername = rs.getString("WAITER");

				TableHistory t = new TableHistory(timestamp, name, itemsInfos, sumOfItems, waiterUsername);
				tablesHistory.add(t);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return tablesHistory;
	}

	public List<Table> getTables(CaffeBL caffe) {
		List<Table> tables = new ArrayList<Table>();
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM STO;");

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String itemsString = rs.getString("items");
				List<Item> items = new ArrayList<Item>();

				if (itemsString.compareTo("") != 0 && itemsString.compareTo("{}") != 0) {
					String[] itemIDS = itemsString.split(",");
					for (String item : itemIDS) {
						items.add(caffe.getItem(Integer.parseInt(item)));
					}
				}

				int x = rs.getInt("X");
				int y = rs.getInt("Y");

				Table t = new Table(id, name, items, x, y);
				tables.add(t);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return tables;
	}

	public void saveTableToHistory(TableHistory t) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("INSERT INTO RACUN (ORDER_TIME, NAME, ITEMS_INFO, SUM_OF_ITEMS, WAITER) VALUES ('" + t.getTimestamp()
				+ "','" + t.getTableName() + "', '" + t.getItemsInfos() + "', " + t.getItemsPrice() + ", '"
				+ t.getWaiterUsername() + "')");

		executeListOFUpdateStatements(sqls);
	}

	public void updateTable(Table t) {
		List<String> sqls = new ArrayList<String>();
		String itemIDs = "";

		if (t.getItems().size() > 0) {
			for (Item item : t.getItems()) {
				itemIDs += item.getID() + ",";
			}
		}

		sqls.add("UPDATE STO SET " + "NAME = '" + t.getName() + "', " + "ITEMS = '" + itemIDs + "', " + "X = "
				+ t.getCoordX() + ", " + "Y = " + t.getCoordY() + " WHERE ID = " + t.getID());

		executeListOFUpdateStatements(sqls);
	}

	public void updateItem(Item i) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("UPDATE STAVKA SET " + "NAME = '" + i.getName() + "', " + "TYPE = " + i.getTypeID() + ", " + "PRICE = "
				+ i.getPrice() + " " + "WHERE ID = " + i.getID());

		executeListOFUpdateStatements(sqls);
	}

	public void updateItemType(ItemType itemType) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("UPDATE TIP_STAVKE SET " + "NAME = '" + itemType.getName() + "' " + "WHERE ID = " + itemType.getID());

		executeListOFUpdateStatements(sqls);
	}

	public void updateUser(String usernameOld, String usernameNew, String passwordNew, boolean isAdmin) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("UPDATE USER SET " + "USERNAME = '" + usernameNew + "', " + "PASSWORD = '" + passwordNew + "', "
				+ "ISADMIN = " + (isAdmin ? 1 : 0) + " " + "WHERE USERNAME = '" + usernameOld + "'");

		executeListOFUpdateStatements(sqls);
	}

	public void insertTable(Table t) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("INSERT INTO STO (ID, NAME, ITEMS, X, Y) VALUES (" + t.getID() + ",'" + t.getName() + "', '', "
				+ t.getCoordX() + ", " + t.getCoordY() + ")");

		executeListOFUpdateStatements(sqls);
	}

	public void insertItem(Item i) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("INSERT INTO STAVKA (CODE, NAME, TYPE, PRICE) VALUES ('" + i.getCode() + "','" + i.getName() + "',"
				+ i.getTypeID() + "," + i.getPrice() + ")");

		executeListOFUpdateStatements(sqls);
	}

	public void insertItemType(ItemType type) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("INSERT INTO TIP_STAVKE (ID, NAME) VALUES (" + type.getID() + ",'" + type.getName() + "')");

		executeListOFUpdateStatements(sqls);
	}

	public void insertUser(String username, String password, boolean isAdmin) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("INSERT INTO USER (USERNAME, PASSWORD, ISADMIN) VALUES ('" + username + "','" + password + "', "
				+ (isAdmin ? 1 : 0) + ")");

		executeListOFUpdateStatements(sqls);
	}

	public void deleteTable(Table t) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("DELETE FROM STO WHERE ID =" + t.getID());
		executeListOFUpdateStatements(sqls);
	}

	public void deleteItem(int ID) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("DELETE FROM STAVKA WHERE ID =" + ID);
		executeListOFUpdateStatements(sqls);
	}

	public void deleteItemType(int ID) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("DELETE FROM TIP_STAVKE WHERE ID =" + ID);
		executeListOFUpdateStatements(sqls);
	}

	public void deleteUser(String username) {
		List<String> sqls = new ArrayList<String>();

		sqls.add("DELETE FROM USER WHERE USERNAME ='" + username + "'");
		executeListOFUpdateStatements(sqls);
	}

	// Helper methods:
	private static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	private static void executeListOFUpdateStatements(List<String> sqls) {
		Connection c = null;
		Statement stmt = null;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
			stmt = c.createStatement();

			for (String sql : sqls) {
				stmt.executeUpdate(sql);
				System.out.println(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
