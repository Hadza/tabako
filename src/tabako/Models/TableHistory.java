package tabako.Models;

import java.util.Calendar;

public class TableHistory {
	private long timestamp;
	private String tableName;
	private String itemsInfos;
	private long itemsPrice;
	private String waiterUsername;

	public TableHistory(long timestamp, String tableName, String itemsInfos, long itemsPrice, String waiterUsername) {
		this.timestamp = timestamp;
		this.tableName = tableName;
		this.itemsInfos = itemsInfos;
		this.itemsPrice = itemsPrice;
		this.waiterUsername = waiterUsername;
	}

	public String getWaiterUsername() {
		return waiterUsername;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getTableName() {
		return tableName;
	}

	public String getItemsInfos() {
		return itemsInfos;
	}

	public long getItemsPrice() {
		return itemsPrice;
	}

	public static TableHistory createTableHistory(Table t, User waiter) {
		String itemIDs = "";
		long sumOfItems = 0;

		if (t.getItems().size() > 0) {
			for (Item item : t.getItems()) {
				itemIDs += item.getName() + ",";
				sumOfItems += item.getPrice();
			}
		}

		return new TableHistory(Calendar.getInstance().getTimeInMillis(), t.getName(), itemIDs, sumOfItems,
				waiter == null ? "" : waiter.getUsername());
	}
}
