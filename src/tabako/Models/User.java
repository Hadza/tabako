package tabako.Models;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import tabako.DBUtils.DB;

public class User {
	private SimpleStringProperty username;
	private SimpleStringProperty password;
	private SimpleBooleanProperty isAdmin;

	public User(String username, String password, boolean isAdmin) {
		this.username = new SimpleStringProperty(username);
		this.password = new SimpleStringProperty(password);
		this.isAdmin = new SimpleBooleanProperty(isAdmin);
	}

	public String getUsername() {
		return username.get();
	}

	public String getPassword() {
		return password.get();
	}

	public boolean isAdmin() {
		return isAdmin.get();
	}

	public void setUsername(String u) {
		DB.getInstance().updateUser(getUsername(), u, getPassword(), isAdmin());
		username.set(u);
	}

	public void setPassword(String p) {
		DB.getInstance().updateUser(getUsername(), getUsername(), p, isAdmin());
		password.set(p);
	}

	public void setIsAdmin(boolean i) {
		DB.getInstance().updateUser(getUsername(), getUsername(), getPassword(), i);
		isAdmin.set(i);
	}
}
