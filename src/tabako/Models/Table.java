package tabako.Models;

import java.util.ArrayList;
import java.util.List;

import tabako.DBUtils.DB;

public class Table {
	// main fields
	public String name;
	private int ID;
	private List<Item> items;

	private double xCoord, yCoord;
	private boolean coordDirty;

	// Constructors
	public Table(int x) {
		ID = x;
		name = "Sto " + ID;
		items = new ArrayList<Item>();
		coordDirty = false;
	}

	public Table(int id, String name, List<Item> items, double x, double y) {
		this.ID = id;
		this.name = name;
		this.items = items;
		xCoord = x;
		yCoord = y;
		coordDirty = false;
	}

	public Table(Table t, List<Item> items) {
		this.ID = t.ID;
		this.name = t.name;
		this.items = items;
		this.xCoord = t.xCoord;
		this.yCoord = t.yCoord;
		this.coordDirty = t.coordDirty;
	}

	// Getters and setters
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String p_name) {
		name = p_name;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> p_items) {
		items = p_items;
	}

	public void setCoords(double x, double y) {
		this.xCoord = x;
		this.yCoord = y;
		coordDirty = true;
	}

	public double getCoordX() {
		return xCoord;
	}

	public double getCoordY() {
		return yCoord;
	}

	// Public methods:
	public long calcSum() {
		long m_itemSum = 0;
		for (int i = 0; i < items.size(); i++) {
			m_itemSum = m_itemSum + items.get(i).getPrice();
		}
		return m_itemSum;
	}

	public void removeLastItem() {
		int size = items.size();

		if (size > 0) {
			items.remove(size - 1);
			DB.getInstance().updateTable(this);
		}
	}

	public void removeItemByCode(String p_code) {
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).getCode().equals(p_code)) {
				items.remove(i);
				break;
			}

		}
		DB.getInstance().updateTable(this);
	}

	public int getItemsSize() {
		return items.size();
	}

	public void removeItems(List<Item> itemsToRemove) {
		// remove items
		for (Item item : itemsToRemove) {
			items.remove(item);
		}

		DB.getInstance().updateTable(this);
	}

	public void removeAllItems() {
		// remove all items
		items.clear();
		DB.getInstance().updateTable(this);
	}

	public boolean hasItems() {
		return items.size() > 0;
	}

	public boolean hasItem(int ID) {
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).getID() == ID) {
				return true;
			}
		}
		return false;
	}

	public void updateTablePosition() {
		if (coordDirty) {
			DB.getInstance().updateTable(this);
			coordDirty = false;
		}
	}

	public void addItem(Item item) {
		items.add(item);
		DB.getInstance().updateTable(this);
	}

	public void removeItem(Item item) {
		items.remove(item);
		DB.getInstance().updateTable(this);
	}

	public Item getItem(int i) {
		return items.get(i);
	}

	public int getAmountOfItem(Item item) {
		int ret = 0;
		for (int i = 0; i < items.size(); i++) {
			if (item.getID() == items.get(i).getID())
				ret++;
		}
		return ret;
	}
}