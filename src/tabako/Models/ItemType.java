package tabako.Models;

public class ItemType {
	private static int posID = 0;
	private int ID;
	private String name;

	public ItemType(int ID, String name) {
		this.ID = ID;
		this.name = name;

		if (ID + 1 > posID) {
			posID = ID + 1;
		}
	}

	public ItemType(String name) {
		this(posID, name);
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(ItemType itemType) {
		return ID == itemType.ID;
	}
}
