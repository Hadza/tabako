package tabako.Models;

public class Item {
	private static int posID = 0;
	private int ID;
	private String name;
	private String code;
	private long price;
	private int typeID;

	public Item(int ID, int typeID, String name, String code, long price) {
		this.ID = ID;
		this.typeID = typeID;
		this.name = name;
		this.code = code;
		this.price = price;

		if (ID + 1 > posID) {
			posID = ID + 1;
		}
	}

	public Item(int typeID, String name, String code, long price) {
		this(posID, typeID, name, code, price);
	}

	// Getters:
	public long getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	public int getID() {
		return ID;
	}

	public int getTypeID() {
		return typeID;
	}

	public String getCode() {
		return code;
	}

	// Setters:
	public void setPriceNameType(long price, String name, int typeID) {
		this.price = price;
		this.name = name;
		this.typeID = typeID;
	}
}