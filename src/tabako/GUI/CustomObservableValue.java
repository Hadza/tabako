package tabako.GUI;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class CustomObservableValue<T> implements ObservableValue<T> {

	public void addListener(InvalidationListener listener) {
	}

	public void removeListener(InvalidationListener listener) {
	}

	public void addListener(ChangeListener<? super T> listener) {
	}

	public void removeListener(ChangeListener<? super T> listener) {
	}

	public T getValue() {
		return null;
	}

}
