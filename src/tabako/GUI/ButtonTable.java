package tabako.GUI;

import javafx.scene.control.Button;
import tabako.Models.Table;

public class ButtonTable extends Button {

	private Table table;

	public ButtonTable(Table t) {
		table = t;
	}

	public Table getTable() {
		return table;
	}
}