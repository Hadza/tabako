package tabako.GUI.Panes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javafx.animation.TranslateTransition;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import tabako.GUI.ButtonItem;
import tabako.GUI.CaffeGUI;
import tabako.Models.Item;
import tabako.Models.ItemType;
import tabako.Models.Table;

public class AddDrinksToTablePane extends BorderPane {
	private double width;
	private double height;
	private CaffeGUI caffe;
	private ArrayList<Tab> tabs;

	public AddDrinksToTablePane(CaffeGUI caffe, double width, double height) {
		super();
		this.caffe = caffe;
		this.width = width;
		this.height = height * 7 / 10;
		tabs = new ArrayList<Tab>();
		initialize();
	}

	public void initialize() {
		setPrefWidth(this.width);
		setPrefHeight(this.height);
		TabPane tabPane = new TabPane();
		setId("add_drinks");
		HashMap<String, FlowPane> typeFlow = new HashMap<String, FlowPane>();

		List<ItemType> itemTypes = caffe.getCaffe().getItemTypes();

		for (int i = 0; i < itemTypes.size(); i++) {
			ItemType itemType = itemTypes.get(i);

			Tab tab = new Tab();
			tab.setId("tab");
			tab.setText(itemType.getName());
			tab.setClosable(false);
			FlowPane flow = new FlowPane();
			List<Item> items = caffe.getCaffe().getItems(itemType.getID());

			for (int j = 0; j < items.size(); j++) {
				ButtonItem itemButton = new ButtonItem(items.get(j));
				itemButton.setText(items.get(j).getName() + "\n" + items.get(j).getPrice());
				itemButton.setTextAlignment(TextAlignment.CENTER);
				itemButton.setPrefSize(width / 4.5, height / 8);
				itemButton.setStyle("-fx-font-size: 140%;");
				itemButton.setId("black-button");
				flow.getChildren().add(itemButton);
				itemButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent e) {
						itemOrdered(e);
					}
				});
			}
			flow.setAlignment(Pos.TOP_LEFT);
			flow.setHgap(width / 50);
			flow.setVgap(height / 70);
			flow.setPadding(new Insets(height / 70, width / 50, height / 70, width / 50));
			tab.setContent(flow);
			tabPane.getTabs().add(tab);
			typeFlow.put(itemType.getName(), flow);
			tabPane.setTabMinWidth(width / 8);
			tabPane.setTabMinHeight(height / 20);
			tabs.add(tab);
		}

		setCenter(tabPane);
		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, width * 3 / 8, height / 12, width * 3 / 8));
		Button back = new Button(">>>");
		back.setId("record-sales");
		back.setPrefWidth(width);
		back.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				TranslateTransition closeNav = new TranslateTransition(new Duration(400), AddDrinksToTablePane.this);
				closeNav.setFromX(0);
				closeNav.setToX(caffe.getRoot().getWidth());
				closeNav.play();
				caffe.closeAddDrinks();
			}
		});
		hbox.getChildren().addAll(back);
		setBottom(hbox);
	}

	public void itemOrdered(ActionEvent e) {
		ButtonItem temp = (ButtonItem) e.getSource();
		caffe.addItem(temp);
	}
}