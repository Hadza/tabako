package tabako.GUI.Panes;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.effect.DropShadow;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import javafx.util.Duration;
import tabako.GUI.CaffeGUI;
import tabako.GUI.CustomObservableValue;
import tabako.Models.TableHistory;
import tabako.Models.User;

public class ViewBills extends BorderPane {
	private double width;
	private double height;
	private CaffeGUI caffe;
	private ArrayList<Tab> tabs;
	private ObservableList<String> sto = FXCollections.observableArrayList();
	private ObservableList<String> waiters = FXCollections.observableArrayList();
	private ObservableList<TableHistory> data = FXCollections.observableArrayList();
	private ScrollPane scrollpane;
	private TableView<TableHistory> tableView = new TableView<TableHistory>();
	private Date date1;
	private Date date2;
	private long sumaItema, brPorudzbina;
	private Label suma, br;
	private DropShadow ds;
	private ComboBox<String> konobari;

	public ViewBills(CaffeGUI caffe, double width, double height) {
		super();
		this.caffe = caffe;
		this.width = width;
		this.height = height * 7 / 10;
		tabs = new ArrayList<Tab>();
		initialize(caffe.getCaffe().getUser());
	}

	public void initialize(User loggedUser) {
		setPrefWidth(this.width);
		setPrefHeight(this.height);
		TabPane tabPane = new TabPane();
		setId("view_history");

		if (loggedUser != null && loggedUser.isAdmin()) {
			waiters.add("Svi");

			for (int i = 0; i < caffe.getCaffe().getAllUsers(true).size(); i++) {
				User user = caffe.getCaffe().getAllUsers(true).get(i);
				waiters.add(user.getUsername());
			}
		} else {
			waiters.add(loggedUser.getUsername());
		}
		Tab tab = new Tab();
		ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.BLACK);
		tab.setId("tab");
		tab.setText("Računi");
		tab.setClosable(false);
		FlowPane flow = new FlowPane();
		flow.setAlignment(Pos.TOP_LEFT);
		// flow.setPadding(new Insets(height/70,width/50,height/70,width/50));
		tab.setContent(flow);
		Label datumod = new Label("Podaci za datum od:");
		datumod.setEffect(ds);
		FlowPane.setMargin(datumod, new Insets(30, 0, 0, 50));
		datumod.setId("dialoglabel");
		datumod.setFont(new Font("Arial", 14));
		datumod.setTextAlignment(TextAlignment.CENTER);
		flow.getChildren().add(datumod);
		DatePicker datefrom = new DatePicker();
		FlowPane.setMargin(datefrom, new Insets(30, 0, 0, 20));
		datefrom.setValue(LocalDate.now());
		datefrom.setEffect(ds);
		datefrom.setOnAction(event -> {
			date1 = Date.from(datefrom.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
			refreshData();
		});
		flow.getChildren().add(datefrom);
		Label datumdo = new Label("do:");
		datumdo.setId("dialoglabel");
		datumdo.setEffect(ds);
		FlowPane.setMargin(datumdo, new Insets(30, 0, 0, 20));
		datumdo.setFont(new Font("Arial", 14));
		datumdo.setTextAlignment(TextAlignment.CENTER);
		flow.getChildren().add(datumdo);
		DatePicker dateto = new DatePicker();
		FlowPane.setMargin(dateto, new Insets(30, 0, 0, 20));
		dateto.setValue(LocalDate.now().minusDays(-1));
		dateto.setEffect(ds);
		dateto.setOnAction(event -> {
			date2 = Date.from(dateto.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
			refreshData();
		});
		flow.getChildren().add(dateto);
		Label konobar = new Label("Konobar:");
		konobar.setEffect(ds);
		FlowPane.setMargin(konobar, new Insets(30, 0, 0, 50));
		konobar.setId("dialoglabel");
		konobar.setFont(new Font("Arial", 14));
		konobar.setTextAlignment(TextAlignment.CENTER);
		flow.getChildren().add(konobar);
		konobari = new ComboBox<String>();
		konobari.setEffect(ds);
		FlowPane.setMargin(konobari, new Insets(30, 0, 0, 50));
		konobari.setItems(waiters);
		konobari.setValue(waiters.get(0));
		konobari.setOnAction(event -> {
			refreshData();
		});
		flow.getChildren().add(konobari);
		scrollpane = new ScrollPane();
		scrollpane.setEffect(ds);
		scrollpane.setPrefSize(width - 50, 5 * height / 6);
		FlowPane.setMargin(scrollpane, new Insets(30, 0, 0, 20));
		flow.getChildren().add(scrollpane);
		addViewTable();
		date1 = Date.from(datefrom.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		date2 = Date.from(dateto.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		tabPane.getTabs().add(tab);
		tabPane.setTabMinWidth(width / 4);
		tabPane.setTabMinHeight(height / 20);
		tabs.add(tab);
		setCenter(tabPane);

		suma = new Label("Suma: ");
		suma.setId("dialoglabel");
		suma.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		suma.setTextAlignment(TextAlignment.CENTER);
		FlowPane.setMargin(suma, new Insets(30, 0, 0, 50));
		flow.getChildren().add(suma);

		br = new Label("Broj porudzbina: ");
		br.setId("dialoglabel");
		br.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		br.setTextAlignment(TextAlignment.CENTER);
		FlowPane.setMargin(br, new Insets(30, 0, 0, 50));
		flow.getChildren().add(br);

		refreshData();

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, width * 3 / 8, height / 12, width * 3 / 8));
		Button back = new Button(">>>");
		back.setId("record-sales");
		back.setEffect(ds);
		back.setPrefWidth(width);
		back.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				caffe.closeViewHistory();
			}
		});
		hbox.getChildren().addAll(back);
		setBottom(hbox);
	}

	void addViewTable() {
		tableView.setEditable(false);
		scrollpane.setContent(tableView);
		tableView.setPrefSize(scrollpane.getPrefWidth(), scrollpane.getPrefHeight());
		TableColumn<TableHistory, String> viewSto = new TableColumn<TableHistory, String>("Sto");
		viewSto.setResizable(false);
		viewSto.setPrefWidth(scrollpane.getPrefWidth() / 8);
		TableColumn<TableHistory, String> waiterName = new TableColumn<TableHistory, String>("Konobar");
		waiterName.setResizable(false);
		waiterName.setPrefWidth(scrollpane.getPrefWidth() / 8);
		TableColumn<TableHistory, Long> viewSuma = new TableColumn<TableHistory, Long>("Suma");
		viewSuma.setResizable(false);
		viewSuma.setPrefWidth(scrollpane.getPrefWidth() / 8);
		TableColumn<TableHistory, String> viewTime = new TableColumn<TableHistory, String>("Vreme");
		viewTime.setResizable(false);
		viewTime.setPrefWidth(scrollpane.getPrefWidth() / 8);
		TableColumn<TableHistory, String> viewLista = new TableColumn<TableHistory, String>("Artikli");
		// setData(FXCollections.observableArrayList());
		viewLista.setPrefWidth(scrollpane.getPrefWidth() / 2);
		viewSto.setCellValueFactory(new Callback<CellDataFeatures<TableHistory, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TableHistory, String> p) {
				ObservableValue<String> observableString = new CustomObservableValue<String>() {
					public String getValue() {
						return p.getValue().getTableName();
					}
				};
				return observableString;
			}
		});
		waiterName.setCellValueFactory(new Callback<CellDataFeatures<TableHistory, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TableHistory, String> p) {
				ObservableValue<String> observableInteger = new CustomObservableValue<String>() {
					public String getValue() {
						return p.getValue().getWaiterUsername();
					}
				};
				return observableInteger;
			}
		});
		viewSuma.setCellValueFactory(new Callback<CellDataFeatures<TableHistory, Long>, ObservableValue<Long>>() {
			public ObservableValue<Long> call(CellDataFeatures<TableHistory, Long> p) {
				ObservableValue<Long> observableLong = new CustomObservableValue<Long>() {
					public Long getValue() {
						return p.getValue().getItemsPrice();
					}
				};
				return observableLong;
			}
		});
		viewTime.setCellValueFactory(new Callback<CellDataFeatures<TableHistory, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TableHistory, String> p) {
				ObservableValue<String> observableLong = new CustomObservableValue<String>() {
					public String getValue() {
						Calendar c = Calendar.getInstance();
						long timestamp = p.getValue().getTimestamp();
						c.setTimeInMillis(timestamp);
						SimpleDateFormat format1 = new SimpleDateFormat("MM.dd. HH:mm");
						String formatted = format1.format(c.getTime());
						return formatted;
					}
				};
				return observableLong;
			}
		});

		viewLista.setCellValueFactory(new Callback<CellDataFeatures<TableHistory, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TableHistory, String> p) {
				ObservableValue<String> observableInteger = new CustomObservableValue<String>() {
					public String getValue() {
						return p.getValue().getItemsInfos();
					}
				};
				return observableInteger;
			}
		});

		viewLista.setCellFactory(new Callback<TableColumn<TableHistory, String>, TableCell<TableHistory, String>>() {
			@Override
			public TableCell<TableHistory, String> call(final TableColumn<TableHistory, String> column) {
				final TableCell cell = new TableCell() {
					private Text text;

					public void updateItem(Object item, boolean empty) {
						super.updateItem(item, empty);
						if (!isEmpty()) {
							String itemString = item.toString();
							text = new Text(itemString.substring(0, itemString.length() - 1));
							text.setWrappingWidth(300);
							setGraphic(text);
						} else {
							setGraphic(null);
						}
					}
				};
				return cell;
			}
		});

		tableView.getColumns().addAll(viewTime, viewSto, waiterName, viewSuma, viewLista);
	}

	private void refreshData() {
		setData(date1.getTime(), date2.getTime());
	}

	private void setData(long t1, long t2) {
		data.clear();
		brPorudzbina = 0;
		sumaItema = 0;
		List<TableHistory> local;
		if (konobari.getValue().toString() == "Svi") {
			local = caffe.getCaffe().getTableHistory(t1, t2, "");
		} else {
			local = caffe.getCaffe().getTableHistory(t1, t2, konobari.getValue().toString());
		}
		brPorudzbina = local.size();
		for (int i = 0; i < local.size(); i++) {
			TableHistory tableHistory = local.get(i);
			data.add(tableHistory);
			sumaItema += tableHistory.getItemsPrice();
		}

		tableView.setItems(data);
		suma.setText("Suma: " + sumaItema);
		suma.setEffect(ds);
		br.setText("Broj porudzbina: " + brPorudzbina);
		br.setEffect(ds);
	}
}
