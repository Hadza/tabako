package tabako.GUI.Panes;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.Models.User;
import tabako.DBUtils.DB;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.effect.DropShadow;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.List;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;

public class AdminPane extends BorderPane {

	private TableView<User> table;
	private ObservableList<User> data;
	private Label errorLabel;
	private CaffeGUI caffe;
	private double width;
	private double height;
	private Label username, password, newUser, newPassword, repeatnewPassword;
	private TextField usertext, newUserText;
	private PasswordField passtext, newpasstext, repeatnewpasstext;
	private DropShadow ds;

	public AdminPane(CaffeGUI caffe, double width, double height) {
		super();
		this.caffe = caffe;
		this.width = width;
		this.height = height * 7 / 10;
		initialize();
	}

	private void initialize() {
		setPrefWidth(this.width);
		setPrefHeight(this.height);
		setId("admin_pane");

		FlowPane flow = new FlowPane();
		setCenter(flow);
		setTableView();
		FlowPane konobarflow = new FlowPane();
		konobarflow.setPrefSize(width / 2.2, height * 2 / 3);
		ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.BLACK);
		// Label Konobari
		Label label = new Label("Konobari");
		label.setEffect(ds);
		FlowPane.setMargin(label, new Insets(height / 24, 0, 0, width / 6));
		label.setId("dialoglabel");
		label.setFont(Font.font("Arial", FontWeight.BOLD, 30));
		flow.getChildren().add(label);
		// Label Admin
		Label admin = new Label("Administrator");
		admin.setId("dialoglabel");
		admin.setEffect(ds);
		admin.setFont(Font.font("Arial", FontWeight.BOLD, 30));
		FlowPane.setMargin(admin, new Insets(height / 24, 0, 0, width / 3));
		flow.getChildren().add(admin);
		flow.getChildren().add(konobarflow);
		// ScrollPane for table
		ScrollPane scrollpane = new ScrollPane();
		scrollpane.setEffect(ds);
		scrollpane.setPrefSize(table.getPrefWidth(), table.getPrefHeight());
		FlowPane.setMargin(scrollpane, new Insets(height / 48, 0, 0, width / 48));
		scrollpane.setContent(table);
		konobarflow.getChildren().addAll(scrollpane);
		// Hbox for admin data
		FlowPane adminData = new FlowPane();
		adminData.setId("admin");
		adminData.setPrefSize(width / 2.2, height * 2 / 3);
		FlowPane.setMargin(adminData, new Insets(height / 48, 0, 0, width / 48));
		username = new Label("Korisničko ime:");
		// Label username
		username.setPrefWidth(adminData.getPrefWidth() / 3);
		username.setId("dialoglabel");
		username.setEffect(ds);
		username.setStyle("-fx-font-size: 130%");
		FlowPane.setMargin(username, new Insets(height / 12, 0, 0, adminData.getPrefWidth() / 24));
		// TextField for username input
		usertext = new TextField("");
		usertext.setEffect(ds);
		usertext.setPrefWidth(adminData.getPrefWidth() / 2.2);
		FlowPane.setMargin(usertext, new Insets(height / 12, 0, 0, 0));
		// Label password
		password = new Label("Šifra:");
		password.setEffect(ds);
		password.setStyle("-fx-font-size: 130%");
		password.setPrefWidth(adminData.getPrefWidth() / 3);
		password.setId("dialoglabel");
		FlowPane.setMargin(password, new Insets(adminData.getPrefHeight() / 24, 0, 0, adminData.getPrefWidth() / 24));
		// PasswordField for password input
		passtext = new PasswordField();
		passtext.setEffect(ds);
		passtext.setPrefWidth(adminData.getPrefWidth() / 2.2);
		FlowPane.setMargin(passtext, new Insets(adminData.getPrefHeight() / 24, 0, 0, 0));
		// Line separator
		Line line = new Line();
		line.setStroke(Color.WHITE);
		line.setStartX(adminData.getLayoutX());
		line.setEndX(adminData.getLayoutX() + adminData.getPrefWidth() * 3 / 4);
		FlowPane.setMargin(line, new Insets(adminData.getPrefHeight() / 24, adminData.getPrefWidth() / 24, 0,
				adminData.getPrefWidth() / 24));
		// New username
		newUser = new Label("Novo korisničko ime:");
		newUser.setEffect(ds);
		newUser.setStyle("-fx-font-size: 130%");
		newUser.setPrefWidth(adminData.getPrefWidth() / 3);
		newUser.setId("dialoglabel");
		FlowPane.setMargin(newUser, new Insets(adminData.getPrefHeight() / 24, 0, 0, adminData.getPrefWidth() / 24));
		// New username textfield
		newUserText = new TextField();
		newUserText.setEffect(ds);
		newUserText.setPrefWidth(adminData.getPrefWidth() / 2.2);
		FlowPane.setMargin(newUserText, new Insets(adminData.getPrefHeight() / 24, 0, 0, 0));
		// New password label
		newPassword = new Label("Nova šifra:");
		newPassword.setEffect(ds);
		newPassword.setStyle("-fx-font-size: 130%");
		newPassword.setPrefWidth(adminData.getPrefWidth() / 3);
		newPassword.setId("dialoglabel");
		FlowPane.setMargin(newPassword,
				new Insets(adminData.getPrefHeight() / 24, 0, 0, adminData.getPrefWidth() / 24));
		// New Password textfield
		newpasstext = new PasswordField();
		newpasstext.setEffect(ds);
		newpasstext.setPrefWidth(adminData.getPrefWidth() / 2.2);
		FlowPane.setMargin(newpasstext, new Insets(adminData.getPrefHeight() / 24, 0, 0, 0));
		// Repeat new password label
		repeatnewPassword = new Label("Ponovite novu šifru:");
		repeatnewPassword.setEffect(ds);
		repeatnewPassword.setStyle("-fx-font-size: 130%");
		repeatnewPassword.setPrefWidth(adminData.getPrefWidth() / 3);
		repeatnewPassword.setId("dialoglabel");
		FlowPane.setMargin(repeatnewPassword,
				new Insets(adminData.getPrefHeight() / 24, 0, 0, adminData.getPrefWidth() / 24));
		// New Password textfield
		repeatnewpasstext = new PasswordField();
		repeatnewpasstext.setEffect(ds);
		repeatnewpasstext.setPrefWidth(adminData.getPrefWidth() / 2.2);
		FlowPane.setMargin(repeatnewpasstext, new Insets(adminData.getPrefHeight() / 24, 0, 0, 0));
		// Button
		Button promeni = new Button("Promeni Admina");
		promeni.setEffect(ds);

		FlowPane.setMargin(promeni, new Insets(adminData.getPrefHeight() / 12, 0, 0, adminData.getPrefWidth() / 6));
		Button potvrdi = new Button("Potvrdi Promenu");
		potvrdi.setEffect(ds);
		potvrdi.setDisable(true);
		promeni.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				setDisableTo(false);
				potvrdi.setDisable(false);
			}
		});
		potvrdi.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				processInputs();
			}
		});
		FlowPane.setMargin(potvrdi, new Insets(adminData.getPrefHeight() / 12, 0, 0, adminData.getPrefWidth() / 24));
		// ADD
		adminData.getChildren().addAll(username, usertext, password, passtext, line);
		adminData.getChildren().addAll(newUser, newUserText, newPassword, newpasstext, repeatnewPassword,
				repeatnewpasstext, promeni, potvrdi);
		setDisableTo(true);
		flow.getChildren().add(adminData);
		// Add and delete buttons
		Button addbtn = new Button("Add");
		addbtn.setEffect(ds);
		FlowPane.setMargin(addbtn, new Insets(height / 48, 0, 0, konobarflow.getPrefWidth() * 3 / 7));
		addbtn.setOnAction(new AddButtonListener());
		Button delbtn = new Button("Delete");
		FlowPane.setMargin(delbtn, new Insets(height / 48, 0, 0, konobarflow.getPrefWidth() / 48));
		delbtn.setOnAction(new DeleteButtonListener());
		delbtn.setEffect(ds);
		konobarflow.getChildren().addAll(addbtn, delbtn);

		errorLabel = new Label("");
		errorLabel.setId("dialoglabel");
		errorLabel.setStyle("-fx-text-fill: red; -fx-font-size: 130%");
		errorLabel.setPrefWidth(width);
		errorLabel.setEffect(ds);
		errorLabel.setAlignment(Pos.CENTER);
		FlowPane.setMargin(errorLabel, new Insets(20, 0, 0, 20));

		Button back = new Button(">>>");
		back.setEffect(ds);
		back.setId("record-sales");

		back.setPrefWidth(width / 6);
		back.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				TranslateTransition closeNav = new TranslateTransition(new Duration(400), AdminPane.this);
				closeNav.setFromX(0);
				closeNav.setToX(caffe.getRoot().getWidth());
				closeNav.play();
				caffe.closeAdminPane();
			}
		});
		FlowPane.setMargin(back, new Insets(height / 3, 0, 0, width * 3 / 7));
		flow.getChildren().addAll(errorLabel, back);
	}

	private void setDisableTo(boolean value) {
		username.setDisable(value);
		password.setDisable(value);
		newUser.setDisable(value);
		newPassword.setDisable(value);
		repeatnewPassword.setDisable(value);
		usertext.setDisable(value);
		newUserText.setDisable(value);
		passtext.setDisable(value);
		newpasstext.setDisable(value);
		repeatnewpasstext.setDisable(value);
	}

	private void processInputs() {
		if (newpasstext.getText().equals("") || newUserText.getText().equals("")
				|| repeatnewpasstext.getText().equals("") || usertext.getText().equals("")) {
			errorLabel.setText("Polja ne mogu biti prazna.");
		} else {
			if (newpasstext.getText().equals(repeatnewpasstext.getText())) {
				User user = DB.getInstance().getUserByUsernameAndPassword(usertext.getText(), passtext.getText());
				if (user == null) {
					errorLabel.setText("Admin ne postoji.");
				} else {
					errorLabel.setText("");
					DB.getInstance().updateUser(usertext.getText(), newUserText.getText(), newpasstext.getText(), true);
					CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Uspešno promenjeni podaci o adminu!");
				}
			} else {
				errorLabel.setText("Ponovljena Nova Šifra i Nova Šifra se ne poklapaju.");
			}
		}
	}

	private void setTableView() {
		// Table view, data, columns and properties
		table = new TableView<User>();
		data = getInitialTableData();
		table.setItems(data);
		table.setEditable(true);

		TableColumn<User, String> usernameCol = new TableColumn<User, String>("Konobar");
		usernameCol.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
		usernameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		usernameCol.setOnEditCommit(new EventHandler<CellEditEvent<User, String>>() {
			public void handle(CellEditEvent t) {
				String newUsername = (String) t.getNewValue();
				if (checkUsernameConstraint(newUsername)) {
					((User) t.getTableView().getItems().get(t.getTablePosition().getRow())).setUsername(newUsername);
				} else {
					errorLabel.setText("Konobar mora biti jedinstven!");
				}
			}
		});

		TableColumn<User, String> passwordCol = new TableColumn<User, String>("PIN");
		passwordCol.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		passwordCol.setCellFactory(TextFieldTableCell.forTableColumn());
		passwordCol.setOnEditCommit(new EventHandler<CellEditEvent<User, String>>() {
			public void handle(CellEditEvent t) {
				String newPassword = (String) t.getNewValue();
				if (checkPasswordConstraint(newPassword)) {
					((User) t.getTableView().getItems().get(t.getTablePosition().getRow())).setPassword(newPassword);
				} else {
					errorLabel.setText("PIN mora biti jedinstven!");
				}
			}
		});
		table.getColumns().setAll(usernameCol, passwordCol);
		table.setPrefWidth(width / 2.2);
		table.setPrefHeight(height * 2 / 3);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		table.getSelectionModel().selectedIndexProperty().addListener(new RowSelectChangeListener());

	}

	private class RowSelectChangeListener implements ChangeListener {

		public void changed(ObservableValue observable, Object oldValue, Object newValue) {
			int ix = ((Number) newValue).intValue();

			if ((ix == data.size())) {
				return; // invalid data
			}

			errorLabel.setText("");
		}
	}

	private ObservableList<User> getInitialTableData() {
		List<User> list = caffe.getCaffe().getAllUsers(false);
		ObservableList<User> data = FXCollections.observableList(list);
		return data;
	}

	private boolean checkPasswordConstraint(String password) {
		for (User u : data) {
			if (u.getPassword().equals(password)) {
				return false;
			}
		}
		return true;
	}

	private boolean checkUsernameConstraint(String username) {
		for (User u : data) {
			if (u.getUsername().equals(username)) {
				return false;
			}
		}
		return true;
	}

	private class AddButtonListener implements EventHandler {

		public void handle(Event event) {
			// Create a new row after last row
			User user = new User("...", "...", false);

			if (!checkUsernameConstraint(user.getUsername()) || !checkPasswordConstraint(user.getPassword())) {
				errorLabel.setText("Prvo podesite \"...\" polja");
				return;
			}

			data.add(user);
			caffe.getCaffe().addUser(user);
			int row = data.size() - 1;

			// Select the new row
			table.requestFocus();
			table.getSelectionModel().select(row);
			table.getFocusModel().focus(row);
			errorLabel.setText("");
		}
	}

	private class DeleteButtonListener implements EventHandler {

		@Override
		public void handle(Event event) {

			// Get selected row and delete
			int ix = table.getSelectionModel().getSelectedIndex();
			User user = (User) table.getSelectionModel().getSelectedItem();
			if (user != null) {
				caffe.getCaffe().removeUser(user.getUsername());
				data.remove(ix);
				CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Obrisan konobar " + user.getUsername());
				errorLabel.setText("");

				// Select a row

				if (table.getItems().size() == 0) {
					errorLabel.setText("Tabela je prazna!");
					return;
				}

				if (ix != 0) {

					ix = ix - 1;
				}

				table.requestFocus();
				table.getSelectionModel().select(ix);
				table.getFocusModel().focus(ix);
			}
		}
	}
}
