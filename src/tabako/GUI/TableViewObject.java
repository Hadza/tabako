package tabako.GUI;

import tabako.Models.Item;

public class TableViewObject {
	private Item item;
	private int numOfItems;

	public TableViewObject(Item i) {
		item = i;
		numOfItems = 1;
	}

	public int getNumOfItems() {
		return numOfItems;
	}

	public Item getItem() {
		return item;
	}

	public void incNumOfItems() {
		numOfItems++;
	}

	public void decNumOfItems() {
		if (numOfItems > 0)
			numOfItems--;
	}
}
