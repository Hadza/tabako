package tabako.GUI;

import tabako.GUI.Dialogs.AddNewFloorPlanDialog;
import tabako.GUI.Dialogs.AddNewItemDialog;
import tabako.GUI.Dialogs.AddTablesDialog;
import tabako.GUI.Dialogs.EditItemDialog;
import tabako.GUI.Dialogs.EditItemTypeDialog;
import tabako.GUI.Dialogs.LoginDialog;
import tabako.GUI.Dialogs.RemoveItemDialog;
import tabako.GUI.Dialogs.RemoveItemTypeDialog;
import tabako.GUI.Dialogs.SplitBillDialog;
import tabako.GUI.Dialogs.WaiterPINDialog;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.GUI.Panes.AddDrinksToTablePane;
import tabako.GUI.Panes.AdminPane;
import tabako.GUI.Panes.ViewBills;
import tabako.DBUtils.DB;
import tabako.BL.CaffeBL;
import tabako.Models.Item;
import tabako.Models.Table;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

public class CaffeGUI {
	private CaffeBL caffe;

	private BorderPane root;
	private Stage mainStage;
	private Scene mainScene;

	private StackPane stackLeft;
	private AnchorPane rootLeft;
	private VBox rootRight;
	private AddDrinksToTablePane addDrinks = null;
	private ViewBills viewHistory = null;
	private AdminPane admin_pane = null;

	private MenuBar menuBar;
	private Menu itemsMenu, tablesMenu, administrationMenu, optionsMenu, logoutMenu, aboutMenu;

	private Label tableNameLabel;
	private Button checkoutButton, addButton, splitBillButton;

	private List<ButtonTable> buttons;

	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private boolean buttonsLocked = true;
	private boolean transferFlag = false;

	private int buttonID = 0;
	private int buttonWidth = 0;
	private int SelectedTable = -1;

	private Table transferTable1 = null;
	private Table transferTable2 = null;

	// table view
	private ScrollPane scrollPane;
	private ObservableList<TableViewObject> data;
	private TableView<TableViewObject> tableView = new TableView<TableViewObject>();

	public int getButtonID() {
		return buttonID;
	}

	public void setButtonID(int id) {
		buttonID = id;
	}

	public CaffeBL getCaffe() {
		return caffe;
	}

	public CaffeGUI(Stage mainStage, CaffeBL caffe) {
		this.mainStage = mainStage;
		this.caffe = caffe;
		buttons = new ArrayList<>();
	}

	public void initialize() {
		// Postavljanje glavne scene. Ona je kao JFrame u java swingu.
		// Svi ostali elementi su njena "deca";
		mainStage.setTitle("Caffe Splav Tabako");
		root = new BorderPane();
		menuBar = new MenuBar();
		root.setTop(menuBar);
		mainScene = new Scene(root);
		mainStage.setScene(mainScene);
		mainStage.getIcons().add(new Image("file:resources/coffe.png"));
		mainStage.setMaximized(true);
		mainStage.show();
		stackLeft = new StackPane();
		rootLeft = new AnchorPane();
		rootLeft.setId("rootLeft");
		rootRight = new VBox();
		rootRight.setSpacing(5);
		rootRight.setId("rootRight");
		rootRight.setPadding(new Insets(7, 10, 5, 10));
		stackLeft.getChildren().add(rootLeft);
		root.setCenter(stackLeft);
		root.setRight(rootRight);
		// setting label for selected table name
		tableNameLabel = new Label();
		tableNameLabel.setTextAlignment(TextAlignment.CENTER);
		tableNameLabel.setPrefSize(0.2 * screenSize.width, 0.01 * rootRight.getHeight());
		tableNameLabel.setId("lbl_TableNames");
		rootRight.getChildren().add(tableNameLabel);
		// setting the Hbox for three buttons
		HBox h = new HBox();
		rootRight.getChildren().add(h);
		h.setSpacing(5);
		// setting button for adding drinks to table
		addButton = new Button("Dodaj Pića");
		addButton.setStyle("-fx-font-size: 130%");
		addButton.setId("black-button");
		addButton.setPrefSize(0.216 * screenSize.width, 60);
		addButton.setDisable(true);
		h.getChildren().add(addButton);
		// btn_Add button listener implementation
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (addDrinks == null) {
					addDrinks = new AddDrinksToTablePane(CaffeGUI.this, rootLeft.getWidth(), rootLeft.getHeight());
					TranslateTransition openNav = new TranslateTransition(new Duration(400), addDrinks);
					openNav.setToX(0);
					openNav.setFromX(rootLeft.getWidth());
					openNav.play();
					stackLeft.getChildren().add(addDrinks);
				} else {
					TranslateTransition closeNav = new TranslateTransition(new Duration(400), addDrinks);
					closeNav.setFromX(0);
					closeNav.setToX(root.getWidth());
					closeNav.play();
					closeAddDrinks();
				}
			}
		});
		// setting button for printing the bill (DO NOT DELETE THIS COMMENTED
		// CODE!)
		/*
		 * Button btn_Print=new Button("Štampaj Račun");
		 * btn_Print.setId("black-button");
		 * btn_Print.setPrefSize(0.07*screenSize.width,100);
		 * btn_Print.setOnAction(new EventHandler<ActionEvent>() {
		 * 
		 * public void handle(ActionEvent e){ //TO BE ADDED! } });
		 * h.getChildren().add(btn_Print); // setting button for opening the
		 * drinks Menu for editing; Button btn_Menu = new Button("Ceo Meni");
		 * btn_Menu.setId("black-button"); btn_Menu.setDisable(true);
		 * btn_Menu.setPrefSize(0.07*screenSize.width,100);
		 * btn_Menu.setOnAction(new EventHandler<ActionEvent>() {
		 * 
		 * public void handle(ActionEvent e){ //TO BE ADDED! } });
		 * h.getChildren().add(btn_Menu);
		 */

		checkoutButton = new Button("Naplati:");
		Button btn_RemoveSelected = new Button("Ukloni selektovano");
		btn_RemoveSelected.setId("black-button");
		btn_RemoveSelected.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (SelectedTable > -1) {
					removeSelectedItem();
					refreshSelectedTableInfo();
				}
			}
		});
		btn_RemoveSelected.setPrefSize(0.216 * screenSize.width, 60);
		btn_RemoveSelected.setStyle("-fx-font-size: 130%");
		btn_RemoveSelected.setDisable(true);
		btn_RemoveSelected.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
		rootRight.getChildren().add(btn_RemoveSelected);
		// Setting the scrollPane for the table
		scrollPane = new ScrollPane();
		rootRight.getChildren().add(scrollPane);
		scrollPane.setPrefWidth(0.216 * screenSize.width);
		scrollPane.setPrefHeight(0.45 * screenSize.height);
		setTableView();
		// setting button for check out, the Sum button;
		checkoutButton.setId("payment-button");
		checkoutButton.setStyle("-fx-font-weight: bold;");
		checkoutButton.setPrefSize(0.216 * screenSize.width, 50);
		// CheckOut button listener implementation;
		checkoutButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (SelectedTable > -1 && caffe.hasTables()) {
					Table selectedtable = caffe.getTables().get(SelectedTable);
					long currentPrice = selectedtable.calcSum();
					if (currentPrice > 0) {
						new WaiterPINDialog(CaffeGUI.this, selectedtable, selectedtable, null);
					}
				}
			}
		});
		checkoutButton.setDisable(true);
		rootRight.getChildren().add(checkoutButton);

		// setting SplitBill button
		splitBillButton = new Button("Odvojena Naplata");
		splitBillButton.setDisable(true);
		splitBillButton.setId("payment-button");
		splitBillButton.setStyle("-fx-font-weight: bold;");
		splitBillButton.setPrefSize(0.216 * screenSize.width, 50);
		// SplitBill button listener implementation;
		splitBillButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent e) {
				if (SelectedTable > -1) {
					new SplitBillDialog(CaffeGUI.this, buttons.get(SelectedTable).getTable());

				}
			}

		});

		rootRight.getChildren().add(splitBillButton);
		refreshSelectedTableInfo();
		mainScene.getStylesheets().add(CaffeGUI.class.getResource("CSS/Stylesheet.css").toExternalForm());
		setTablesBack();
		createMenu();
		setupMenus(false);
	}

	public void setTableData(Table p_table) {
		data.clear();
		for (int i = 0; i < p_table.getItemsSize(); i++) {
			boolean itemFound = false;
			TableViewObject currentItem = new TableViewObject(p_table.getItem(i));
			for (int j = 0; j < data.size(); j++) {
				if (currentItem.getItem().equals(data.get(j).getItem())) {
					data.get(j).incNumOfItems();
					itemFound = true;
					break;
				}
			}
			if (itemFound == false)
				data.add(currentItem);
		}

		tableView.setItems(data);
	}

	public void removeSelectedItem() {
		if (tableView.getSelectionModel().getSelectedIndex() != -1) {
			if (caffe.getTable(SelectedTable).hasItems()) {
				TableViewObject removeItem = tableView.getSelectionModel().getSelectedItem();
				if (removeItem.getNumOfItems() > 1)
					removeItem.decNumOfItems();
				else
					data.remove(removeItem);
				String code = removeItem.getItem().getCode();
				caffe.getTable(SelectedTable).removeItemByCode(code);
			}
		}
		tableView.setItems(data);
		tableView.refresh();

	}

	class Delta {
		double x, y;
	}

	public void setTableView() {
		tableView.setEditable(false);
		scrollPane.setContent(tableView);
		tableView.setPrefSize(scrollPane.getPrefWidth(), scrollPane.getPrefHeight());
		TableColumn<TableViewObject, String> itemName = new TableColumn<TableViewObject, String>("Naziv");
		itemName.setResizable(false);
		TableColumn<TableViewObject, Integer> itemPrice = new TableColumn<TableViewObject, Integer>("Cena");
		itemPrice.setResizable(false);
		TableColumn<TableViewObject, Integer> itemAmount = new TableColumn<TableViewObject, Integer>("Količina");
		itemAmount.setResizable(false);
		itemName.setPrefWidth(tableView.getPrefWidth() * 1.85 / 5);
		itemPrice.setPrefWidth(tableView.getPrefWidth() * 1.85 / 5);
		itemAmount.setPrefWidth(tableView.getPrefWidth() / 5);
		data = FXCollections.observableArrayList();
		itemName.setCellValueFactory(
				new Callback<CellDataFeatures<TableViewObject, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<TableViewObject, String> p) {
						ObservableValue<String> observableString = new CustomObservableValue<String>() {
							public String getValue() {
								return p.getValue().getItem().getName();
							}
						};
						return observableString;
					}
				});
		itemPrice.setCellValueFactory(
				new Callback<CellDataFeatures<TableViewObject, Integer>, ObservableValue<Integer>>() {
					public ObservableValue<Integer> call(CellDataFeatures<TableViewObject, Integer> p) {
						ObservableValue<Integer> observableInteger = new CustomObservableValue<Integer>() {
							public Integer getValue() {
								return (int) p.getValue().getItem().getPrice();
							}
						};
						return observableInteger;
					}
				});
		itemAmount.setCellValueFactory(
				new Callback<CellDataFeatures<TableViewObject, Integer>, ObservableValue<Integer>>() {
					public ObservableValue<Integer> call(CellDataFeatures<TableViewObject, Integer> p) {
						ObservableValue<Integer> observableInteger = new CustomObservableValue<Integer>() {
							public Integer getValue() {
								return p.getValue().getNumOfItems();
							}
						};
						return observableInteger;
					}
				});
		tableView.getColumns().add(itemName);
		tableView.getColumns().add(itemPrice);
		tableView.getColumns().add(itemAmount);
	}

	public void transferTables(Table p_transf1, Table p_transf2) {
		List<Item> tmp;
		tmp = p_transf1.getItems();
		p_transf1.setItems(p_transf2.getItems());
		p_transf2.setItems(tmp);
		transferFlag = false;
		DB.getInstance().updateTable(p_transf1);
		DB.getInstance().updateTable(p_transf2);
		CustomAlert.showAndWait(AlertType.INFORMATION, "Transfer", "Transfer je uspešno izvršen!");
	}

	public void addNewTable(Table t, boolean restore) {
		buttonID = buttons.size();
		if (!restore) {
			caffe.addTable(t);
			buttonsLocked = false;
		}

		ButtonTable astal = new ButtonTable(t);

		if (buttonID == 0) {
			astal.setText("Šank");
			astal.getTable().setName("Šank");
		} else {
			astal.setText("Sto" + buttonID);
		}

		astal.setId("stolovi");
		astal.setPrefWidth(65);
		buttons.add(astal);
		if (restore == false) {
			astal.setLayoutY(5);
			astal.setLayoutX(buttonWidth + 5);
			buttonWidth = (int) (buttonWidth + astal.getPrefWidth() + 5);
			if (buttonWidth > rootLeft.getWidth() - astal.getPrefWidth())
				buttonWidth = 0;

		} else {
			astal.setLayoutX(astal.getTable().getCoordX());
			astal.setLayoutY(astal.getTable().getCoordY());
			if (t.hasItems()) {
				astal.setStyle("-fx-border-color: red;");
			}
		}
		rootLeft.getChildren().add(astal);
		final Delta dragDelta = new Delta();
		astal.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent mouseEvent) {
				if (!buttonsLocked) {
					dragDelta.x = astal.getLayoutX() - mouseEvent.getSceneX();
					dragDelta.y = astal.getLayoutY() - mouseEvent.getSceneY();
					astal.setCursor(Cursor.MOVE);
				}
			}
		});
		astal.setOnRotate(e -> {
			astal.setRotate(astal.getRotate() + e.getAngle());
			e.consume();
		});
		astal.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent mouseEvent) {
				if (!buttonsLocked) {
					astal.setCursor(Cursor.HAND);
					astal.getTable().updateTablePosition();
				}
			}
		});
		astal.setOnMouseDragged(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent mouseEvent) {
				if (!buttonsLocked) {
					astal.setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
					astal.setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
					astal.getTable().setCoords(mouseEvent.getSceneX() + dragDelta.x,
							mouseEvent.getSceneY() + dragDelta.y);
				}
			}
		});
		astal.setOnMouseEntered(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent mouseEvent) {
				if (!buttonsLocked)
					astal.setCursor(Cursor.HAND);
			}
		});
		astal.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				ButtonTable btnTemp;
				Object source = e.getSource();
				btnTemp = (ButtonTable) source;
				SelectedTable = btnTemp.getTable().getID();
				refreshSelectedTableInfo();
				// in case of selection for transfer

				if (transferFlag && transferTable1 == null) {
					transferTable1 = caffe.getTable(SelectedTable);

				} else if (transferFlag && transferTable1 != null && transferTable2 == null) {
					transferTable2 = caffe.getTable(SelectedTable);
					transferTables(transferTable1, transferTable2);
					if (transferTable1.hasItems())
						buttons.get(transferTable1.getID()).setStyle("-fx-border-color: red;");
					else
						buttons.get(transferTable1.getID()).setStyle("-fx-border-color: green;");
					if (transferTable2.hasItems())
						buttons.get(transferTable2.getID()).setStyle("-fx-border-color: red;");
					else
						buttons.get(transferTable2.getID()).setStyle("-fx-border-color: green;");

					transferTable1 = null;
					transferTable2 = null;
				}

			}
		});
	}

	public void createMenu() {
		tablesMenu = new Menu("Stolovi");
		MenuItem dodaj = new MenuItem("Dodaj Stolove");
		dodaj.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new AddTablesDialog(CaffeGUI.this);
			}
		});

		MenuItem postavljeni = new MenuItem(buttonsLocked ? "Otključaj Stolove!" : "Zaključaj Stolove!");
		postavljeni.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				buttonsLocked = !buttonsLocked;
				postavljeni.setText(buttonsLocked ? "Otključaj Stolove!" : "Zaključaj Stolove!");
			}
		});

		MenuItem transfer = new MenuItem("Promena Stola");
		transfer.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Optional<ButtonType> result = CustomAlert.showAndWait(AlertType.CONFIRMATION, "Transfer",
						"Izaberite dva stola koja želite da zamenite");
				if (result.get() == ButtonType.OK) {
					transferFlag = true;
				}
			}
		});

		tablesMenu.getItems().addAll(dodaj, postavljeni, transfer);

		itemsMenu = new Menu("Artikli");

		MenuItem addItems = new MenuItem("Dodaj u Jelovnik");
		addItems.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new AddNewItemDialog(CaffeGUI.this);
			}
		});

		Menu remove = new Menu("Ukloni iz Jelovnika");

		MenuItem removeItems = new MenuItem("Artikle");
		removeItems.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new RemoveItemDialog(CaffeGUI.this);
			}
		});

		MenuItem removeCategory = new MenuItem("Kategorije Artikala");
		removeCategory.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new RemoveItemTypeDialog(CaffeGUI.this);
			}
		});
		remove.getItems().addAll(removeItems, removeCategory);

		Menu edit = new Menu("Izmeni");

		MenuItem editItems = new MenuItem("Artikle");
		editItems.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new EditItemDialog(CaffeGUI.this);
			}
		});

		MenuItem editCategory = new MenuItem("Kategorije Artikala");
		editCategory.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new EditItemTypeDialog(CaffeGUI.this);
			}
		});

		edit.getItems().addAll(editItems, editCategory);
		itemsMenu.getItems().addAll(addItems, remove, edit);

		optionsMenu = new Menu("Nalog");

		MenuItem login = new MenuItem("Login");
		login.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new LoginDialog(CaffeGUI.this, true);
			}
		});

		optionsMenu.getItems().addAll(login);

		administrationMenu = new Menu("Administracija");

		MenuItem setPicture = new MenuItem("Promena FloorPlan-a");
		setPicture.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new AddNewFloorPlanDialog(CaffeGUI.this);
			}
		});

		MenuItem korisnici = new MenuItem("Podešavanje Naloga");
		korisnici.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (admin_pane == null) {
					admin_pane = new AdminPane(CaffeGUI.this, rootLeft.getWidth(), rootLeft.getHeight());
					TranslateTransition openNav = new TranslateTransition(new Duration(400), admin_pane);
					openNav.setToX(0);
					openNav.setFromX(rootLeft.getWidth());
					openNav.play();
					stackLeft.getChildren().add(admin_pane);
				}
			}
		});

		MenuItem istorija = new MenuItem("Pregled Računa");
		istorija.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				showBills();
			}
		});

		administrationMenu.getItems().addAll(setPicture, korisnici, istorija);

		logoutMenu = new Menu("Nalog");

		MenuItem adminLogout = new MenuItem("Logout");
		adminLogout.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				setupMenus(false);
				closeAllPanes();
				CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Uspešno ste se izlogovali.");
			}
		});

		logoutMenu.getItems().addAll(adminLogout);

		aboutMenu = new Menu("O programu");
		MenuItem verzija = new MenuItem("Verzija");
		verzija.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				CustomAlert.showAndWait(AlertType.INFORMATION, "Verzija",
						"Verzija Programa: \n 02.00.00.00\n JFX enabled.");
			}
		});

		MenuItem autori = new MenuItem("Autori");
		autori.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				CustomAlert.showAndWait(AlertType.INFORMATION, "Autori",
						"Petar Ješić: petarjeshic@hotmail.com \nMiloš Hadžić: donadog@gmail.com");
			}
		});

		aboutMenu.getItems().addAll(verzija, autori);
	}

	public void showBills() {
		if (viewHistory == null) {
			viewHistory = new ViewBills(CaffeGUI.this, rootLeft.getWidth(), rootLeft.getHeight());
			TranslateTransition openNav = new TranslateTransition(new Duration(400), viewHistory);
			openNav.setToX(0);
			openNav.setFromX(rootLeft.getWidth());
			openNav.play();
			stackLeft.getChildren().add(viewHistory);
		}
	}

	public void setupMenus(boolean isAdmin) {
		// clear all menu items
		menuBar.getMenus().clear();
		if (isAdmin) {
			menuBar.getMenus().addAll(tablesMenu, itemsMenu, administrationMenu, logoutMenu, aboutMenu);
		} else {
			menuBar.getMenus().addAll(tablesMenu, optionsMenu, aboutMenu);
		}
	}

	// vraca stolove na mesto gde su bili pre gasenja programa
	public void setTablesBack() {
		for (int i = 0; i < caffe.getTables().size(); i++) {
			Table table = caffe.getTable(i);
			rootLeft.setLayoutX(table.getCoordX());
			rootLeft.setLayoutY(table.getCoordY());
			addNewTable(table, true);
		}
	}

	public void addItem(ButtonItem temp) {
		boolean itemFound = false;

		caffe.getTable(SelectedTable).addItem(temp.getItem());
		for (int j = 0; j < data.size(); j++) {
			if (temp.getItem() == (data.get(j).getItem())) {
				data.get(j).incNumOfItems();
				itemFound = true;
				break;
			}
		}

		if (itemFound == false) {
			TableViewObject tableItem = new TableViewObject(temp.getItem());
			data.add(tableItem);
		}
		refreshSelectedTableInfo();
	}

	public void refreshSelectedTableInfo() {
		tableView.refresh();

		if (SelectedTable > -1 && caffe.hasTables()) {
			Table table = caffe.getTables().get(SelectedTable);
			setTableData(table);

			tableNameLabel.setText(table.getName());

			addButton.setDisable(false);
			tableView.setDisable(false);

			checkoutButton.setText("Naplati: " + table.calcSum() + ".00");

			if (table.hasItems()) {
				buttons.get(SelectedTable).setStyle("-fx-border-color: red;");
				checkoutButton.setDisable(false);
				splitBillButton.setDisable(table.getItemsSize() == 1);
			} else {
				buttons.get(SelectedTable).setStyle("-fx-border-color: green;");
				checkoutButton.setDisable(true);
				splitBillButton.setDisable(true);
			}

		} else {
			tableNameLabel.setText("Izaberite");

			addButton.setDisable(true);
			tableView.setDisable(true);

			checkoutButton.setText("Naplati:");

			checkoutButton.setDisable(true);
			splitBillButton.setDisable(true);

		}
	}

	public boolean removeLastTable() {
		if (buttons.size() > 0) {
			int index = buttons.size() - 1;
			if (caffe.getTable(index).getID() == SelectedTable) {
				refreshSelectedTableInfo();
			}

			if (caffe.removeLastTable()) {
				rootLeft.getChildren().remove(index);
				buttons.remove(index);
				buttonWidth = buttonWidth - 70;
				if (buttonWidth < 0) {
					buttonWidth = (int) rootLeft.getWidth() - 70;
				}
				return true;
			}
		}
		return false;
	}

	public void closeAllPanes() {
		closeViewHistory();
		closeAdminPane();
		closeAddDrinks();
	}

	public void closeViewHistory() {
		TranslateTransition closeNav = new TranslateTransition(new Duration(400), viewHistory);
		closeNav.setFromX(0);
		closeNav.setToX(root.getWidth());
		closeNav.play();
		viewHistory = null;
	}

	public void closeAdminPane() {
		TranslateTransition closeNav = new TranslateTransition(new Duration(400), admin_pane);
		closeNav.setFromX(0);
		closeNav.setToX(root.getWidth());
		closeNav.play();
		admin_pane = null;
	}

	public void closeAddDrinks() {
		TranslateTransition closeNav = new TranslateTransition(new Duration(400), addDrinks);
		closeNav.setFromX(0);
		closeNav.setToX(root.getWidth());
		closeNav.play();
		addDrinks = null;
	}

	public List<ButtonTable> getButtons() {
		return buttons;
	}

	public BorderPane getRoot() {
		return root;
	}

	public AnchorPane getRootLeft() {
		return rootLeft;
	}
}