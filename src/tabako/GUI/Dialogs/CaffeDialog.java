package tabako.GUI.Dialogs;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CloseButtonListener;
import tabako.GUI.Dialogs.helpers.EscapeButtonListener;

public abstract class CaffeDialog extends Dialog<CaffeGUI> {
	// logic
	protected CaffeGUI caffe;

	// view
	protected FlowPane root;
	protected Stage dialogStage;
	protected Scene dialogScene;
	protected Label errorLabel;
	protected Button closeButton;

	public static String ICON_PATH = "file:resources/coffe.png";

	protected CaffeDialog(CaffeGUI caffe, double width, double height) {
		// logic
		this.caffe = caffe;

		// root
		root = new FlowPane();
		root.setPrefHeight(height);
		root.setPrefWidth(width);
		root.setHgap(10);
		root.setVgap(10);
		root.setId("dialog");

		// dialog stage
		dialogStage = new Stage();
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		dialogStage.getIcons().add(new Image(ICON_PATH));
		dialogStage.setResizable(false);
		dialogStage.initStyle(StageStyle.TRANSPARENT);

		// dialog scene
		dialogScene = new Scene(root);
		dialogScene.getStylesheets().add(CaffeDialog.class.getResource("../CSS/Stylesheet.css").toExternalForm());
		dialogScene.setFill(Color.TRANSPARENT);
		dialogScene.setOnKeyPressed(new EscapeButtonListener(dialogStage));
		dialogStage.setScene(dialogScene);

		// close button
		closeButton = new Button("Close");

		closeButton.setPrefWidth(100);
		closeButton.setOnAction(new CloseButtonListener(dialogStage));

		// error label
		errorLabel = new Label("");
		FlowPane.setMargin(errorLabel, new Insets(0, 20, 0, 20));
		errorLabel.setStyle("-fx-text-fill: red;");
		errorLabel.setPrefWidth(260);
	}
}
