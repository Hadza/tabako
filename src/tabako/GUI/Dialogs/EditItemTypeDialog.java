package tabako.GUI.Dialogs;

import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.Models.ItemType;

public class EditItemTypeDialog extends CaffeDialog {
	private Label nameLabel;
	private Label customLabel;
	private Label newNameLabel;
	private ComboBox<String> itemTypesList;
	private TextField newNameText;
	private ObservableList<String> options = FXCollections.observableArrayList();
	private Button saveButton;

	public EditItemTypeDialog(CaffeGUI caffe) {
		super(caffe, 300, 300);

		// custom Label
		customLabel = new Label("Odaberite tip artikla koji se ažurira");
		customLabel.setId("dialoglabel");
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 20));

		// name label
		nameLabel = new Label("Naziv:");
		nameLabel.setId("dialoglabel");
		nameLabel.setPrefWidth(70);
		FlowPane.setMargin(nameLabel, new Insets(0, 5, 0, 20));

		// item types
		List<ItemType> itemTypes = caffe.getCaffe().getItemTypes();
		for (int i = 0; i < itemTypes.size(); i++)
			options.add(itemTypes.get(i).getName());
		itemTypesList = new ComboBox<String>(options);
		itemTypesList.setPromptText("Odaberite iz liste.");
		itemTypesList.setPrefWidth(150);
		FlowPane.setMargin(itemTypesList, new Insets(0, 20, 0, 20));

		// newName label
		newNameLabel = new Label("Novi naziv:");
		newNameLabel.setId("dialoglabel");
		newNameLabel.setPrefWidth(70);
		FlowPane.setMargin(newNameLabel, new Insets(0, 5, 0, 20));

		// newName text
		newNameText = new TextField("");
		newNameText.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					handleInputs();
				}
			}
		});
		newNameText.setPrefWidth(150);
		FlowPane.setMargin(newNameText, new Insets(0, 20, 0, 20));

		// save button
		saveButton = new Button("Ažuriraj");
		FlowPane.setMargin(saveButton, new Insets(20, 20, 0, 20));
		saveButton.setPrefWidth(100);
		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				handleInputs();
			}
		});

		FlowPane.setMargin(closeButton, new Insets(20, 20, 0, 20));
		root.getChildren().addAll(customLabel, nameLabel, itemTypesList, newNameLabel, newNameText, errorLabel,
				saveButton, closeButton);

		dialogStage.show();
	}

	private void handleInputs() {

		if (itemTypesList.getValue() == null) {
			errorLabel.setText("Morate odabrati kategoriju");
			return;
		}

		if (newNameText.getText().isEmpty()) {
			errorLabel.setText("Morate postaviti novi naziv");
			return;
		}

		if (caffe.getCaffe().getItemType(newNameText.getText()) != null) {
			errorLabel.setText("Kategorija \"" + newNameText.getText() + "\" već postoji.");
			return;
		}

		// edit in logic
		caffe.getCaffe().updateItemType(itemTypesList.getValue().toString(), newNameText.getText());

		// show alert
		CustomAlert.showAndWait(AlertType.INFORMATION, "Uspešno ste ažurirali kategoriju!",
				"Stari naziv \"" + itemTypesList.getValue() + "\", novi naziv \"" + newNameText.getText() + "\"");

		// refresh view
		errorLabel.setText("");
		itemTypesList.getItems().clear();
		List<ItemType> lista = caffe.getCaffe().getItemTypes();
		for (int i = 0; i < lista.size(); i++) {
			options.add(lista.get(i).getName());
		}
	}
}
