package tabako.GUI.Dialogs;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.FlowPane;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.Models.ItemType;
import tabako.BL.CaffeBL.RemoveItemMessage;

public class RemoveItemTypeDialog extends CaffeDialog {
	private Label customLabel;
	private Label typeLabel;
	private ComboBox<String> itemTypesList;
	private ObservableList<String> options = FXCollections.observableArrayList();
	private Button removeButton;

	public RemoveItemTypeDialog(CaffeGUI caffe) {
		super(caffe, 300, 300);

		// custom label
		customLabel = new Label("Odaberite tip artikla koji se uklanja");
		customLabel.setId("dialoglabel");
		customLabel.setPrefWidth(300);
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 20));

		// type label
		typeLabel = new Label("Naziv:");
		typeLabel.setId("dialoglabel");
		typeLabel.setPrefWidth(70);
		FlowPane.setMargin(typeLabel, new Insets(0, 5, 0, 20));

		// list of types
		List<ItemType> itemTypes = caffe.getCaffe().getItemTypes();
		for (int i = 0; i < itemTypes.size(); i++)
			options.add(itemTypes.get(i).getName());
		itemTypesList = new ComboBox<String>(options);
		itemTypesList.setPromptText("Odaberite iz liste.");
		itemTypesList.setPrefWidth(150);
		FlowPane.setMargin(itemTypesList, new Insets(0, 20, 0, 20));

		// remove button
		removeButton = new Button("Ukloni");
		FlowPane.setMargin(removeButton, new Insets(20, 20, 0, 20));
		removeButton.setPrefWidth(100);
		removeButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				handleInputs();
			}
		});

		FlowPane.setMargin(closeButton, new Insets(20, 20, 0, 20));
		root.getChildren().addAll(customLabel, typeLabel, itemTypesList, errorLabel, removeButton, closeButton);

		dialogStage.show();
	}

	private void handleInputs() {
		if (itemTypesList.getValue() == null) {
			errorLabel.setText("Morate odabrati kategoriju");
			return;
		}

		RemoveItemMessage result = caffe.getCaffe().removeItemType(itemTypesList.getValue().toString());
		if (result.equals(RemoveItemMessage.ErrorDoesntExist)) {
			errorLabel.setText("Neuspešno! \nKategorija \"" + itemTypesList.getValue() + "\" ne postoji.");
			return;
		}

		if (result.equals(RemoveItemMessage.ErrorInUse)) {
			errorLabel.setText(
					"Neuspešno! \nPrvo obrišite sve artikle pod kategorijom: \n\"" + itemTypesList.getValue() + "\"");
			return;
		}

		if (result.equals(RemoveItemMessage.Success)) {
			// show alert
			CustomAlert.showAndWait(AlertType.INFORMATION, "Info",
					"Uspešno ste uklonili kategoriju \"" + itemTypesList.getValue() + "\" iz restorana.");

			// refresh view
			errorLabel.setText("");
			itemTypesList.getItems().clear();
			List<ItemType> lista = caffe.getCaffe().getItemTypes();
			for (int i = 0; i < lista.size(); i++)
				options.add(lista.get(i).getName());
		}
	}
}
