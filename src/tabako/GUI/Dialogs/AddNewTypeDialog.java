package tabako.GUI.Dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import tabako.GUI.CaffeGUI;

public class AddNewTypeDialog extends CaffeDialog {
	private AddNewItemDialog superdialog;
	private Label typeLabel;
	private TextField typeText;
	private Button actionButton;

	public AddNewTypeDialog(AddNewItemDialog superdialog, CaffeGUI caffe) {
		super(caffe, 300, 200);
		this.superdialog = superdialog;
		root.setId("blackdialog");
		// type label
		typeLabel = new Label("Naziv Tipa:");
		typeLabel.setId("dialoglabel");
		typeLabel.setPrefWidth(200);
		FlowPane.setMargin(typeLabel, new Insets(0, 20, 0, 20));

		// type text
		typeText = new TextField("");
		typeText.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					handleInput();
				}
			}
		});
		typeText.setPrefWidth(200);
		FlowPane.setMargin(typeText, new Insets(0, 20, 0, 20));

		// add type button
		actionButton = new Button("Dodaj novi tip");
		FlowPane.setMargin(actionButton, new Insets(0, 20, 0, 20));
		actionButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				handleInput();
			}
		});

		root.getChildren().addAll(typeLabel, typeText, errorLabel, actionButton, closeButton);

		dialogStage.show();

	}

	private void handleInput() {
		String regex = "[\\s\\p{L}/-]*";
		String inputString = typeText.getText();

		if (inputString.equals("")) {
			errorLabel.setText("Niste uneli naziv!");
			return;
		}

		if (!inputString.matches(regex)) {
			errorLabel.setText("Nedozvoljena slova u nazivu!");
			return;
		}

		if (caffe.getCaffe().getItemType(inputString) != null) {
			errorLabel.setText("Kategorija \"" + inputString + "\" već postoji.");
			return;
		}

		// refresh view
		caffe.getCaffe().addItemType(inputString);
		superdialog.refreshOptions();

		dialogStage.close();

	}
}
