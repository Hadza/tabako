package tabako.GUI.Dialogs;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CloseButtonListener;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.GUI.Dialogs.helpers.EscapeButtonListener;
import tabako.Models.Item;
import tabako.Models.ItemType;

public class EditItemDialog extends Dialog<CaffeGUI> {
	// logic
	private CaffeGUI caffe;
	private Item result;

	// view
	private FlowPane root;
	private Stage dialogStage;
	private Scene dialogScene;
	private ObservableList<String> options = FXCollections.observableArrayList();
	private Label customLabel, codeOrNameLabel, errorLabel, firstErrorLabel, newNameLabel, typeLabel, priceLabel;
	private TextField codeOrNameText, newNameText, priceText;
	private ComboBox<String> listOfTypes;
	private Button findButton, saveButton, closeButton, cancelButton;

	public EditItemDialog(CaffeGUI caffe) {
		// logic
		this.caffe = caffe;

		// root
		root = new FlowPane();
		root.setPrefHeight(500);
		root.setPrefWidth(350);
		root.setHgap(10);
		root.setVgap(10);
		root.setId("dialog");

		// dialog stage
		dialogStage = new Stage();
		dialogStage.getIcons().add(new Image(CaffeDialog.ICON_PATH));
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		dialogStage.setResizable(false);
		dialogStage.initStyle(StageStyle.TRANSPARENT);

		List<ItemType> lista = caffe.getCaffe().getItemTypes();
		for (int i = 0; i < lista.size(); i++)
			options.add(lista.get(i).getName());

		// dialog scene
		dialogScene = new Scene(root);
		dialogScene.getStylesheets().add(AddNewItemDialog.class.getResource("../CSS/Stylesheet.css").toExternalForm());
		dialogScene.setOnKeyPressed(new EscapeButtonListener(dialogStage));
		dialogScene.setFill(Color.TRANSPARENT);
		dialogStage.setScene(dialogScene);

		// custom label
		customLabel = new Label("Unesite naziv ili šifru artikla koji želite\nda izmenite.");
		customLabel.setId("dialoglabel");
		customLabel.setPrefWidth(250);
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 20));
		root.getChildren().addAll(customLabel);

		// codeOrName label
		codeOrNameLabel = new Label("Naziv/Šifra:");
		codeOrNameLabel.setId("dialoglabel");
		codeOrNameLabel.setPrefWidth(100);
		FlowPane.setMargin(codeOrNameLabel, new Insets(0, 5, 0, 20));
		root.getChildren().addAll(codeOrNameLabel);

		// codeOrName text
		codeOrNameText = new TextField("");
		codeOrNameText.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					findItems();
				}
			}
		});
		codeOrNameText.setPrefWidth(150);
		FlowPane.setMargin(codeOrNameText, new Insets(0, 20, 0, 5));
		root.getChildren().addAll(codeOrNameText);

		// error label
		errorLabel = new Label("");
		FlowPane.setMargin(errorLabel, new Insets(0, 20, 0, 20));
		errorLabel.setStyle("-fx-text-fill: red;");
		errorLabel.setPrefWidth(250);

		// error label
		firstErrorLabel = new Label("");
		FlowPane.setMargin(firstErrorLabel, new Insets(0, 20, 0, 20));
		firstErrorLabel.setStyle("-fx-text-fill: red;");
		firstErrorLabel.setPrefWidth(300);
		root.getChildren().addAll(firstErrorLabel);

		// find button
		findButton = new Button("Pronađi");
		FlowPane.setMargin(findButton, new Insets(0, 20, 0, 20));
		findButton.setPrefWidth(100);
		findButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				findItems();
			}
		});
		root.getChildren().addAll(findButton);

		// cancel button
		cancelButton = new Button("Zatvori");
		FlowPane.setMargin(cancelButton, new Insets(0, 20, 0, 20));
		cancelButton.setPrefWidth(100);
		cancelButton.setOnAction(new CloseButtonListener(dialogStage));
		root.getChildren().addAll(cancelButton);

		// newName label
		newNameLabel = new Label("Novi naziv:");
		newNameLabel.setId("dialoglabel");
		FlowPane.setMargin(newNameLabel, new Insets(0, 20, 0, 20));
		newNameLabel.setPrefWidth(250);

		// newName text
		newNameText = new TextField("");
		newNameText.setPrefWidth(250);
		FlowPane.setMargin(newNameText, new Insets(0, 20, 0, 20));

		// type label
		typeLabel = new Label("Tip Artikla:");
		typeLabel.setId("dialoglabel");
		typeLabel.setPrefWidth(250);
		FlowPane.setMargin(typeLabel, new Insets(0, 20, 0, 20));

		// list of types
		listOfTypes = new ComboBox<String>(options);
		listOfTypes.setPromptText("Odaberite iz liste.");
		listOfTypes.setPrefWidth(150);
		FlowPane.setMargin(listOfTypes, new Insets(0, 20, 0, 20));

		// price label
		priceLabel = new Label("Cena:");
		priceLabel.setId("dialoglabel");
		priceLabel.setPrefWidth(250);
		FlowPane.setMargin(priceLabel, new Insets(0, 20, 0, 20));

		// price text
		priceText = new TextField("");
		priceText.setPrefWidth(250);
		FlowPane.setMargin(priceText, new Insets(0, 20, 0, 20));

		// save button
		saveButton = new Button("Ažuriraj");
		FlowPane.setMargin(saveButton, new Insets(0, 20, 0, 20));
		saveButton.setPrefWidth(100);
		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				saveItem();
			}
		});

		// close button
		closeButton = new Button("Zatvori");
		FlowPane.setMargin(closeButton, new Insets(0, 20, 0, 20));
		closeButton.setPrefWidth(100);
		closeButton.setOnAction(new CloseButtonListener(dialogStage));

		dialogStage.show();
	}

	private void findItems() {
		String input = codeOrNameText.getText();
		result = caffe.getCaffe().getItem(input);

		if (result == null) {
			firstErrorLabel.setText("Neuspešno! Artikal \"" + input + "\" ne postoji!");
			return;
		}
		firstErrorLabel.setText("");
		customLabel.setDisable(true);
		codeOrNameLabel.setDisable(true);
		codeOrNameText.setDisable(true);
		findButton.setDisable(true);
		cancelButton.setDisable(true);
		newNameText.setText(result.getName());

		ItemType itemType = caffe.getCaffe().getItemType(result.getTypeID());
		listOfTypes.setValue(itemType.getName());
		priceText.setText(Long.toString(result.getPrice()));

		root.getChildren().addAll(newNameLabel, newNameText, typeLabel, listOfTypes, priceLabel, priceText, errorLabel,
				saveButton, closeButton);
	}

	private void saveItem() {
		// first check code
		String regex = "[\\p{L}0-9.,\\s\\-]*";
		String inputString = newNameText.getText();
		String priceString = priceText.getText();

		if (inputString.equals("") || priceString.equals("")) {
			errorLabel.setText("Nijedno polje ne sme biti prazno!");
			return;
		}
		if (!inputString.matches(regex)) {
			errorLabel.setText("Nedozvoljena slova u nazivu!");
			return;
		}

		if (!priceString.matches("[0-9]*")) {
			errorLabel.setText("Nedozvoljena slova u ceni!");
			return;
		}

		if (listOfTypes.getValue() == null) {
			errorLabel.setText("Niste izabrali tip!");
			return;
		}

		// SUCCESS
		caffe.getCaffe().updateItem(result.getCode(), inputString, Long.parseLong(priceText.getText()),
				listOfTypes.getValue().toString());
		CustomAlert.showAndWait(AlertType.INFORMATION, "Info",
				"Uspesno izmenjen artikal: " + newNameText.getText() + " " + priceText.getText());

		dialogStage.close();
	}
}
