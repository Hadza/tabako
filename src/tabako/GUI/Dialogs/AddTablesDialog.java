package tabako.GUI.Dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.control.Dialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CloseButtonListener;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.GUI.Dialogs.helpers.EscapeButtonListener;
import tabako.Models.Table;

public class AddTablesDialog extends Dialog<CaffeGUI> {
	// view
	private VBox root;
	private Stage dialogStage;
	private Scene dialogScene;
	private Label customLabel;
	private Button closeButton;
	private Button addTableButton;
	private Button removeTableButton;
	private HBox hbox;

	public AddTablesDialog(CaffeGUI caffe) {
		// root
		root = new VBox();
		root.setSpacing(20);
		root.setId("dialog");

		// dialog stage
		dialogStage = new Stage();
		dialogStage.getIcons().add(new Image(CaffeDialog.ICON_PATH));
		dialogStage.initModality(Modality.APPLICATION_MODAL);
		dialogStage.setResizable(false);
		dialogStage.initStyle(StageStyle.TRANSPARENT);

		// dialog scene
		dialogScene = new Scene(root);
		dialogScene.setOnKeyPressed(new EscapeButtonListener(dialogStage));
		dialogScene.getStylesheets().add(AddNewItemDialog.class.getResource("../CSS/Stylesheet.css").toExternalForm());
		dialogScene.setFill(Color.TRANSPARENT);
		dialogStage.setScene(dialogScene);

		// info text
		customLabel = new Label("Dodajte nove stolove u kafić ili uklonite višak iz njega.");
		VBox.setMargin(customLabel, new Insets(20, 20, 0, 20));
		customLabel.setId("dialoglabel");

		// hbox
		hbox = new HBox();
		root.setPadding(new Insets(30, 0, 30, 0));
		hbox.setSpacing(15);
		hbox.setPadding(new Insets(0, 0, 0, 50));
		root.getChildren().addAll(customLabel, hbox);

		// add table button
		addTableButton = new Button("Dodaj Sto");
		addTableButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				caffe.setButtonID(caffe.getButtons().size());
				Table t = new Table(caffe.getButtonID());
				// poziva pravljenje stola prvi put(restore=false)
				caffe.addNewTable(t, false);
			}
		});

		// remove table button
		removeTableButton = new Button("Ukloni Sto");
		removeTableButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (caffe.removeLastTable()) {
					caffe.refreshSelectedTableInfo();
				} else {
					CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Ne možete ukloniti koji imaju artikle.");
				}
			}
		});

		// close button
		closeButton = new Button("Zatvori");
		closeButton.setOnAction(new CloseButtonListener(dialogStage));

		hbox.getChildren().addAll(addTableButton, removeTableButton, closeButton);

		dialogStage.show();
	}
}
