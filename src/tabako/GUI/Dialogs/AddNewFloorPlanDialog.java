package tabako.GUI.Dialogs;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import tabako.GUI.CaffeGUI;

public class AddNewFloorPlanDialog extends CaffeDialog {
	// view
	private Label customLabel;
	private Button browseButton;
	private final FileChooser fileChooser = new FileChooser();

	public AddNewFloorPlanDialog(CaffeGUI caffe) {
		super(caffe, 350, 150);

		// custom label
		customLabel = new Label("Unesite novu pozadinsku sliku koja ce biti korišćena\n kao FloorPlan restorana");
		customLabel.setId("dialoglabel");
		FlowPane.setMargin(customLabel, new Insets(20, 20, 0, 20));

		// browse button
		browseButton = new Button("Browse");
		FlowPane.setMargin(browseButton, new Insets(0, 0, 40, 120));
		configureFileChooser();
		browseButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				File file = fileChooser.showOpenDialog(dialogStage);
				if (file != null) {
					Path path = Paths.get("resources", "", "background.jpg");
					String writeLocation = path.toAbsolutePath().toString();
					String imageFile = new String("");
					try {
						imageFile = file.toURI().toURL().toString();
					} catch (MalformedURLException e1) {
						e1.printStackTrace();
					}
					Image image = new Image(imageFile);
					File output = new File(writeLocation);
					try {
						ImageIO.write(SwingFXUtils.fromFXImage(image, null), "jpg", output);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					// new BackgroundSize(width, height, widthAsPercentage,
					// heightAsPercentage, contain, cover)
					BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
					// new BackgroundImage(image, repeatX, repeatY, position,
					// size)
					BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
							BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSize);
					// new Background(images...)
					Background background = new Background(backgroundImage);
					caffe.getRoot().getStylesheets().clear();
					caffe.getRootLeft().setBackground(background);
				}
			}
		});

		FlowPane.setMargin(closeButton, new Insets(0, 0, 40, 0));
		root.getChildren().addAll(customLabel, browseButton, closeButton);

		// show dialog
		dialogStage.show();
	}

	private void configureFileChooser() {
		fileChooser.setTitle("View Pictures");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"), new FileChooser.ExtensionFilter("PNG", "*.png"));
	}
}
