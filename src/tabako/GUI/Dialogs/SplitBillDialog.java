package tabako.GUI.Dialogs;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;
import tabako.GUI.CaffeGUI;
import tabako.GUI.CustomObservableValue;
import tabako.Models.Item;
import tabako.Models.Table;

public class SplitBillDialog extends CaffeDialog {
	// logic
	private List<Item> leftTableItems;
	private List<Item> rightTableItems;
	private long checkoutSum;

	// view
	private TableView<Item> tableViewLeft;
	private TableView<Item> tableViewRigth;
	private ScrollPane scrollPaneLeft;
	private ScrollPane scrollPaneRight;
	private Button moveLeftButton;
	private Button moveRightButton;
	private Button checkoutButton;

	public SplitBillDialog(CaffeGUI caffe, Table leftTable) {
		super(caffe, 850, 500);
		// initialize logic
		leftTableItems = new ArrayList<Item>(leftTable.getItems());
		rightTableItems = new ArrayList<Item>();
		checkoutSum = 0;

		// move right button
		moveRightButton = new Button(">>");
		moveRightButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Item selectedItem = tableViewLeft.getSelectionModel().getSelectedItem();
				if (selectedItem != null) {
					rightTableItems.add(selectedItem);
					leftTableItems.remove(selectedItem);
					checkoutSum += selectedItem.getPrice();
					refreshView();
				}
			}
		});

		// move left button
		moveLeftButton = new Button("<<");
		moveLeftButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Item selectedItem = tableViewRigth.getSelectionModel().getSelectedItem();
				if (selectedItem != null) {
					leftTableItems.add(selectedItem);
					rightTableItems.remove(selectedItem);
					checkoutSum -= selectedItem.getPrice();
					refreshView();
				}
			}
		});

		// checkout button
		checkoutButton = new Button("Naplati: ");
		checkoutButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if (!rightTableItems.isEmpty()) {
					if (checkoutSum > 0) {
						Table tempTable = new Table(leftTable, rightTableItems);
						new WaiterPINDialog(caffe, leftTable, tempTable, dialogStage);
						refreshView();
					}
				}
			}
		});

		// tables with scrolls
		tableViewLeft = new TableView<Item>();
		scrollPaneLeft = new ScrollPane();
		tableViewRigth = new TableView<Item>();
		scrollPaneRight = new ScrollPane();

		initializeTableView(tableViewLeft, scrollPaneLeft);
		initializeTableView(tableViewRigth, scrollPaneRight);
		root.getChildren().addAll(tableViewLeft, scrollPaneLeft, closeButton, moveRightButton, moveLeftButton,
				tableViewRigth, scrollPaneRight, checkoutButton);
		refreshView();

		// show dialog
		dialogStage.show();
	}

	private void refreshView() {
		// refresh tables contents
		tableViewLeft.setItems(FXCollections.observableArrayList(leftTableItems));
		tableViewRigth.setItems(FXCollections.observableArrayList(rightTableItems));

		// refresh checkout button text
		checkoutButton.setText("Naplati: " + checkoutSum + ".00");
	}

	private void initializeTableView(TableView<Item> p_tblView, ScrollPane p_spPane) {
		p_tblView.setEditable(false);
		p_spPane.setContent(p_tblView);
		p_spPane.setPrefWidth(250);
		p_spPane.setPrefHeight(500);
		p_tblView.setPrefSize(p_spPane.getPrefWidth(), p_spPane.getPrefHeight());
		TableColumn<Item, String> itemName = new TableColumn<Item, String>("Ime");
		itemName.setResizable(false);
		TableColumn<Item, Integer> itemPrice = new TableColumn<Item, Integer>("Cena");
		itemPrice.setResizable(false);
		itemName.setPrefWidth(p_tblView.getPrefWidth() * 1.85 / 3);
		itemPrice.setPrefWidth(p_tblView.getPrefWidth() * 1.85 / 5);

		itemName.setCellValueFactory(new Callback<CellDataFeatures<Item, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<Item, String> p) {
				ObservableValue<String> observableString = new CustomObservableValue<String>() {
					public String getValue() {
						return p.getValue().getName();
					}
				};
				return observableString;
			}
		});
		itemPrice.setCellValueFactory(new Callback<CellDataFeatures<Item, Integer>, ObservableValue<Integer>>() {
			public ObservableValue<Integer> call(CellDataFeatures<Item, Integer> p) {
				ObservableValue<Integer> observableInteger = new CustomObservableValue<Integer>() {
					public Integer getValue() {
						return (int) p.getValue().getPrice();
					}
				};
				return observableInteger;
			}
		});

		p_tblView.getColumns().add(itemName);
		p_tblView.getColumns().add(itemPrice);
	}
}
