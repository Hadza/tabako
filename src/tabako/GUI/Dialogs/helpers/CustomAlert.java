package tabako.GUI.Dialogs.helpers;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class CustomAlert extends Alert {
	public CustomAlert(AlertType alertType) {
		super(alertType);
	}

	public static Optional<ButtonType> showAndWait(AlertType alertType, String title, String message) {
		CustomAlert alert = new CustomAlert(alertType);

		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		if (stage != null) {
			stage.getIcons().add(new Image("file:resources/coffe.png"));
		}

		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(message);
		return alert.showAndWait();
	}
}
