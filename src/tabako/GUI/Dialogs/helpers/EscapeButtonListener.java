package tabako.GUI.Dialogs.helpers;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class EscapeButtonListener implements EventHandler<KeyEvent> {
	private Stage dialogStage;

	public EscapeButtonListener(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void handle(KeyEvent event) {
		if (event.getCode().equals(KeyCode.ESCAPE)) {
			dialogStage.close();
		}
	}
}
