package tabako.GUI.Dialogs.helpers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class CloseButtonListener implements EventHandler<ActionEvent> {
	private Stage dialogStage;

	public CloseButtonListener(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void handle(ActionEvent e) {
		dialogStage.close();
	}
}
