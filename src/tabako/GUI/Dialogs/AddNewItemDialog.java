package tabako.GUI.Dialogs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.FlowPane;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;

public class AddNewItemDialog extends CaffeDialog {
	// logic
	private ObservableList<String> options = FXCollections.observableArrayList();

	// view
	private TextField nameText;
	private TextField priceText;
	private TextField codeText;
	private Label codeLabel;
	private Label name;
	private ComboBox<String> itemTypes;
	private Button actionButton;

	public AddNewItemDialog(CaffeGUI caffe) {
		super(caffe, 320, 400);

		// code label & text
		codeLabel = new Label("Šifra:");
		codeLabel.setId("dialoglabel");
		FlowPane.setMargin(codeLabel, new Insets(20, 20, 0, 20));
		codeLabel.setPrefWidth(250);
		codeText = new TextField("");
		codeText.setPrefWidth(250);
		FlowPane.setMargin(codeText, new Insets(0, 20, 0, 20));

		// name label & text
		name = new Label("Naziv:");
		name.setId("dialoglabel");
		FlowPane.setMargin(name, new Insets(0, 20, 0, 20));
		name.setPrefWidth(250);
		nameText = new TextField("");
		nameText.setPrefWidth(250);
		FlowPane.setMargin(nameText, new Insets(0, 20, 0, 20));

		// types of items
		Label typeLabel = new Label("Tip Artikla:");
		typeLabel.setId("dialoglabel");
		typeLabel.setPrefWidth(250);
		FlowPane.setMargin(typeLabel, new Insets(0, 20, 0, 20));
		itemTypes = new ComboBox<String>();
		refreshOptions();
		itemTypes.setPromptText("Odaberite iz liste.");
		FlowPane.setMargin(itemTypes, new Insets(0, 20, 0, 20));

		// add new type button
		Button addNewTypeButton = new Button("Dodaj novi tip");
		addNewTypeButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				new AddNewTypeDialog(AddNewItemDialog.this, caffe);
			}
		});

		// price label & text
		Label priceLabel = new Label("Cena:");
		priceLabel.setId("dialoglabel");
		priceLabel.setPrefWidth(250);
		FlowPane.setMargin(priceLabel, new Insets(0, 20, 0, 20));
		priceText = new TextField("");
		priceText.setPrefWidth(250);
		FlowPane.setMargin(priceText, new Insets(0, 20, 0, 20));

		// add new item button
		actionButton = new Button("Dodaj Artikal");
		FlowPane.setMargin(actionButton, new Insets(30, 20, 0, 20));
		actionButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				handleInputs();
			}
		});

		FlowPane.setMargin(closeButton, new Insets(30, 20, 0, 20));

		root.getChildren().addAll(codeLabel, codeText, name, nameText, priceLabel, priceText, typeLabel, itemTypes,
				addNewTypeButton, errorLabel, actionButton, closeButton);

		dialogStage.show();
	}

	public void refreshOptions() {
		options.clear();

		for (int i = 0; i < caffe.getCaffe().getItemTypes().size(); i++) {
			String option = caffe.getCaffe().getItemTypes().get(i).getName();
			options.add(option);
		}

		itemTypes.setItems(options);
	}

	private void handleInputs() {
		// first check code
		String regex = "[\\p{L}0-9.,\\s\\-]*";

		String codeString = codeText.getText();
		String inputString = nameText.getText();
		String priceString = priceText.getText();

		if (codeString.equals("") || inputString.equals("") || priceString.equals("")) {
			errorLabel.setText("Nijedno polje ne sme biti prazno!");
			return;
		}

		if (!codeString.matches(regex)) {
			errorLabel.setText("Nedozvoljena slova u sifri!");
			return;
		}

		if (!inputString.matches(regex)) {
			errorLabel.setText("Nedozvoljena slova u nazivu!");
			return;
		}

		if (!priceString.matches("[0-9]*")) {
			errorLabel.setText("Nedozvoljena slova u ceni!");
			return;
		}

		if (itemTypes.getValue() == null) {
			errorLabel.setText("Niste izabrali tip!");
			return;
		}

		if (caffe.getCaffe().getItemByName(inputString) != null) {
			errorLabel.setText("Artikal pod nazivom \"" + inputString + "\" već postoji.");
			return;
		}

		// SUCCESS
		caffe.getCaffe().addItem(itemTypes.getValue().toString(), inputString, codeString, Long.parseLong(priceString));
		errorLabel.setText("");
		CustomAlert.showAndWait(AlertType.INFORMATION, "Info",
				"Uspesno dodat artikal:\n" + nameText.getText() + " u bazu!");

		codeText.clear();
		nameText.clear();
		priceText.clear();
		itemTypes.arm();
	}
}