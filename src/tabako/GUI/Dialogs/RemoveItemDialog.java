package tabako.GUI.Dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.BL.CaffeBL.RemoveItemMessage;

public class RemoveItemDialog extends CaffeDialog {
	private TextField nameOrCodeText;
	private Label customLabel;
	private Label nameOrCodeLabel;
	private Button removeButton;

	public RemoveItemDialog(CaffeGUI caffe) {
		super(caffe, 350, 300);

		// custom label
		customLabel = new Label("Unesite naziv ili šifru artikla koji se uklanja\n sa jelovnika.");
		customLabel.setPrefWidth(300);
		customLabel.setId("dialoglabel");
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 20));

		// nameOrCode label
		nameOrCodeLabel = new Label("Naziv/Šifra:");
		nameOrCodeLabel.setPrefWidth(100);
		nameOrCodeLabel.setId("dialoglabel");
		FlowPane.setMargin(nameOrCodeLabel, new Insets(0, 5, 0, 20));

		// nameOrCode text
		nameOrCodeText = new TextField("");
		nameOrCodeText.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					handleInputs();
				}
			}
		});
		nameOrCodeText.setPrefWidth(150);
		FlowPane.setMargin(nameOrCodeText, new Insets(0, 20, 0, 5));

		// remove button
		removeButton = new Button("Ukloni");
		FlowPane.setMargin(removeButton, new Insets(20, 20, 0, 20));
		removeButton.setPrefWidth(100);
		removeButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				handleInputs();
			}
		});

		FlowPane.setMargin(closeButton, new Insets(20, 20, 0, 20));
		root.getChildren().addAll(customLabel, nameOrCodeLabel, nameOrCodeText, errorLabel, removeButton, closeButton);

		dialogStage.show();
	}

	private void handleInputs() {
		String input = nameOrCodeText.getText();
		if (input.equals("")) {
			errorLabel.setText("Neuspešno! Niste uneli naziv ili šifru.");
			return;
		}

		RemoveItemMessage result = caffe.getCaffe().removeItem(input);
		nameOrCodeText.clear();
		if (result.equals(RemoveItemMessage.ErrorDoesntExist)) {
			errorLabel.setText("Neuspešno! Artikal sa datim nazivom ili\n šifrom ne postoji.");
			return;
		}

		if (result.equals(RemoveItemMessage.ErrorInUse)) {
			errorLabel.setText("Neuspešno! Prvo naplatite svim stolovima\n koji su artikal poručili.");
			return;
		}

		if (result.equals(RemoveItemMessage.Success)) {
			errorLabel.setText("");

			// show alert
			CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Uspešno ste uklonili Artikal iz restorana.");
		}
	}
}
