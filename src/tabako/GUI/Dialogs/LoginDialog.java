package tabako.GUI.Dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextAlignment;
import tabako.GUI.CaffeGUI;
import tabako.GUI.Dialogs.helpers.CustomAlert;
import tabako.Models.User;

public class LoginDialog extends CaffeDialog {
	private Label customLabel;
	private Label usernameLabel;
	private Label passwordLabel;
	private PasswordField passwordtext;
	private TextField usernametext;
	private Button loginButton;

	public LoginDialog(CaffeGUI caffe, boolean showBills) {
		super(caffe, 300, 300);

		// custom label
		customLabel = new Label("Unesite korisničko ime i šifru\n(PIN konobara)");
		customLabel.setTextAlignment(TextAlignment.CENTER);
		customLabel.setId("dialoglabel");
		customLabel.setPrefWidth(300);
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 50));

		// username label
		usernameLabel = new Label("Korisničko ime:");
		usernameLabel.setPrefWidth(100);
		usernameLabel.setTextAlignment(TextAlignment.CENTER);
		usernameLabel.setId("dialoglabel");
		FlowPane.setMargin(usernameLabel, new Insets(20, 0, 0, 40));

		// username text
		usernametext = new TextField("");
		usernametext.setPrefWidth(100);
		usernametext.setPromptText("Vaše korisničko ime");
		FlowPane.setMargin(usernametext, new Insets(20, 0, 0, 5));

		// password label
		passwordLabel = new Label("Šifra: ");
		passwordLabel.setPrefWidth(100);
		passwordLabel.setTextAlignment(TextAlignment.CENTER);
		passwordLabel.setId("dialoglabel");
		FlowPane.setMargin(passwordLabel, new Insets(15, 0, 0, 40));

		// password text
		passwordtext = new PasswordField();
		passwordtext.setPrefWidth(100);
		passwordtext.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					checkdata(showBills);
				}
			}
		});
		passwordtext.setPrefWidth(100);
		passwordtext.setPromptText("Vaša šifra");
		FlowPane.setMargin(passwordtext, new Insets(15, 20, 0, 5));

		// login button
		loginButton = new Button("Login");
		loginButton.setPrefWidth(70);
		FlowPane.setMargin(loginButton, new Insets(15, 0, 0, 50));
		loginButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				checkdata(showBills);
			}
		});

		FlowPane.setMargin(closeButton, new Insets(15, 20, 0, 10));
		root.getChildren().addAll(customLabel, usernameLabel, usernametext, passwordLabel, passwordtext, errorLabel,
				loginButton, closeButton);

		dialogStage.show();
	}

	private void checkdata(boolean showBills) {
		if (usernametext.getText().equals("")) {
			errorLabel.setText("Niste uneli korisničko ime!");
			return;
		}
		if (passwordtext.getText().equals("")) {
			errorLabel.setText("Niste uneli šifru!");
			return;
		}

		User user = caffe.getCaffe().login(usernametext.getText(), passwordtext.getText());

		if (user == null) {
			errorLabel.setText("Pogrešna šifra ili korisničko ime.");
		} else {
			if (user.isAdmin()) {
				CustomAlert.showAndWait(AlertType.INFORMATION, "Info", "Uspešno ste se ulogovali.");
			}

			// hack
			if (showBills && !user.isAdmin()) {
				caffe.showBills();
			}

			dialogStage.close();
			caffe.setupMenus(user.isAdmin());
		}
	}
}
