package tabako.GUI.Dialogs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import tabako.GUI.CaffeGUI;
import tabako.Models.Table;
import tabako.Models.User;

public class WaiterPINDialog extends CaffeDialog {
	private Label customLabel;
	private Label pinLabel;
	private TextField pintext;
	private Button checkoutButton;
	private Stage parentDialog;

	public WaiterPINDialog(CaffeGUI caffe, Table table, Table tmp_table, Stage parentDialog) {
		super(caffe, 250, 300);
		this.parentDialog = parentDialog;

		if (parentDialog != null) {
			root.setId("blackdialog");
		}

		// custom label
		customLabel = new Label("Unesite PIN!");
		customLabel.setTextAlignment(TextAlignment.CENTER);
		customLabel.setId("dialoglabel");
		customLabel.setPrefWidth(300);
		FlowPane.setMargin(customLabel, new Insets(50, 20, 0, 50));

		// PIN label
		pinLabel = new Label("PIN:");
		pinLabel.setTextAlignment(TextAlignment.CENTER);
		pinLabel.setId("dialoglabel");
		FlowPane.setMargin(pinLabel, new Insets(20, 0, 0, 40));

		// PIN text
		pintext = new PasswordField();
		pintext.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() > 4)
				pintext.setText(oldValue);
		});
		pintext.setPrefWidth(130);
		pintext.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					checkdata(table, tmp_table);
				}
			}
		});
		pintext.setPromptText("Vaš PIN");
		FlowPane.setMargin(pintext, new Insets(20, 0, 0, 5));

		// checkout button
		checkoutButton = new Button("Naplati!");
		checkoutButton.setPrefWidth(70);
		FlowPane.setMargin(checkoutButton, new Insets(15, 0, 0, 50));
		checkoutButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				checkdata(table, tmp_table);
			}
		});
		FlowPane.setMargin(closeButton, new Insets(15, 0, 0, 15));
		root.getChildren().addAll(customLabel, pinLabel, pintext, errorLabel, checkoutButton, closeButton);

		dialogStage.show();
	}

	private void checkdata(Table table, Table tmp_table) {
		String PIN = pintext.getText();

		if (PIN.equals("")) {
			errorLabel.setText("Niste uneli PIN.");
			return;
		}

		User waiter = caffe.getCaffe().getUser(PIN);
		if (waiter == null) {
			errorLabel.setText("Ne postoji korisnik sa datim PIN-om.");
			return;
		}

		// pay
		caffe.getCaffe().pay(table, tmp_table, waiter);

		// close dialog
		dialogStage.close();

		// close parent
		if (parentDialog != null) {
			parentDialog.close();
		}

		// refresh data for selected table
		caffe.refreshSelectedTableInfo();
	}
}
