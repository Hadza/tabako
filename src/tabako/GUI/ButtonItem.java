package tabako.GUI;

import javafx.scene.control.Button;
import tabako.Models.Item;

public class ButtonItem extends Button {
	private Item item;

	public Item getItem() {
		return item;
	}

	public ButtonItem(Item item) {
		super();
		this.item = item;
	}
}
