package tabako.main;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import tabako.BL.CaffeBL;
import tabako.GUI.CaffeGUI;

public class Main extends Application {
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage mainStage) {
		try {
			
			CaffeBL coffeBL=new CaffeBL();
			if (coffeBL.checkMacAddress()) {
				// if device is whitelisted, run application:
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							CaffeGUI m_caffeGUI = new CaffeGUI(mainStage,coffeBL);
							m_caffeGUI.initialize();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				System.exit(0);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
