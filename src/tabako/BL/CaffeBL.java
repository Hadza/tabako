package tabako.BL;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.*;
import tabako.DBUtils.DB;
import tabako.Models.Item;
import tabako.Models.ItemType;
import tabako.Models.Table;
import tabako.Models.TableHistory;
import tabako.Models.User;

public class CaffeBL {
	private List<Table> tables;
	private List<Item> menu;
	private List<ItemType> types;
	private User user;

	public CaffeBL() {
		types = DB.getInstance().getItemTypes();
		menu = DB.getInstance().getItems();
		tables = DB.getInstance().getTables(this);
	}

	// Getters and setters
	public List<Table> getTables() {
		return tables;
	}

	public List<Item> getItems() {
		return menu;
	}

	public List<ItemType> getItemTypes() {
		return types;
	}

	public User getUser() {
		return user;
	}

	public List<TableHistory> getTableHistory(long t1, long t2, String username) {
		return DB.getInstance().getTablesHistory(username, t1, t2);
	}

	// TODO: Solve this better
	// Helpers
	public ItemType getItemType(String typeName) {
		for (int i = 0; i < types.size(); i++) {
			if (types.get(i).getName().toLowerCase().equals(typeName.toLowerCase())) {
				return types.get(i);
			}
		}

		return null;
	}

	// Public methods:
	public void addTable(Table t) {
		tables.add(t);
		DB.getInstance().insertTable(t);
	}

	public boolean addItem(String typeName, String name, String code, long price) {
		// first find type ID
		ItemType itemType = getItemType(typeName);
		if (itemType == null) {
			return false;
		}

		Item item = new Item(itemType.getID(), name, code, price);
		menu.add(item);
		DB.getInstance().insertItem(item);
		return true;
	}

	public void addItemType(String name) {
		ItemType itemType = new ItemType(name);
		types.add(itemType);
		DB.getInstance().insertItemType(itemType);
	}

	private void addTableHistory(Table t, User user) {
		TableHistory tableHistory = TableHistory.createTableHistory(t, user);
		DB.getInstance().saveTableToHistory(tableHistory);
	}

	public void addUser(User user) {
		DB.getInstance().insertUser(user.getUsername(), user.getPassword(), user.isAdmin());
	}

	public boolean removeLastTable() {
		int size = tables.size();

		if (size > 0) {
			Table t = tables.get(size - 1);
			if (!t.hasItems()) {
				DB.getInstance().deleteTable(t);
				tables.remove(size - 1);
				return true;
			}
		}

		return false;
	}

	public enum RemoveItemMessage {
		ErrorDoesntExist, ErrorInUse, Success
	}

	public Item getItem(String nameOrCode) {
		for (Item item : menu) {
			if (item.getName().toLowerCase().equals(nameOrCode.toLowerCase())
					|| item.getCode().toLowerCase().equals(nameOrCode.toLowerCase())) {
				return item;
			}
		}

		return null;
	}

	public RemoveItemMessage removeItem(String nameOrCode) {
		// first check whether item exists
		Item itemToBeRemoved = getItem(nameOrCode);
		if (itemToBeRemoved == null) {
			return RemoveItemMessage.ErrorDoesntExist;
		}

		// then check if there is table with that item
		for (Table t : tables) {
			if (t.hasItem(itemToBeRemoved.getID())) {
				return RemoveItemMessage.ErrorInUse;
			}
		}

		DB.getInstance().deleteItem(itemToBeRemoved.getID());
		menu.remove(itemToBeRemoved);
		return RemoveItemMessage.Success;
	}

	public RemoveItemMessage removeItemType(String typeName) {
		ItemType type = getItemType(typeName);
		if (type == null) {
			return RemoveItemMessage.ErrorDoesntExist;
		}

		if (!getItems(type.getID()).isEmpty()) {
			return RemoveItemMessage.ErrorInUse;
		}

		DB.getInstance().deleteItemType(type.getID());
		types.remove(type);
		return RemoveItemMessage.Success;
	}

	public Table getTable(int index) {
		return tables.get(index);
	}

	public boolean hasTables() {
		return tables.size() > 0;
	}

	public Item getItem(int ID) {
		for (Item item : menu) {
			if (item.getID() == ID) {
				return item;
			}
		}
		return null;
	}

	public Item getItemByName(String name) {
		for (Item item : menu) {
			if (item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}

	public ItemType getItemType(int ID) {
		for (ItemType itemType : types) {
			if (itemType.getID() == ID) {
				return itemType;
			}
		}
		return null;
	}

	public List<Item> getItems(int typeID) {
		List<Item> items = new ArrayList<Item>();

		for (int i = 0; i < menu.size(); i++) {
			Item item = menu.get(i);

			if (typeID == item.getTypeID()) {
				items.add(item);
			}
		}
		return items;
	}

	public void updateItem(String nameOrCode, String name, long price, String typeName) {
		String nameOrCodeLowerCase = nameOrCode.toLowerCase();
		for (Item item : menu) {
			if (item.getCode().toLowerCase().equals(nameOrCodeLowerCase)
					|| item.getName().toLowerCase().equals(nameOrCodeLowerCase)) {
				item.setPriceNameType(price, name, getItemType(typeName).getID());
				DB.getInstance().updateItem(item);
				break;
			}
		}
	}

	public void updateItemType(String oldTypeName, String newTypeName) {
		ItemType itemType = getItemType(oldTypeName);
		itemType.setName(newTypeName);
		DB.getInstance().updateItemType(itemType);
	}

	public void pay(Table original, Table payed, User waiter) {
		// first save payed items to history
		addTableHistory(payed, waiter);

		// remove items from original table
		if (original != payed) {
			original.removeItems(payed.getItems());
		} else {
			original.removeAllItems();
		}
	}

	public User getUser(String password) {
		return DB.getInstance().getUserByPassword(password);
	}

	public User login(String username, String password) {
		if (username == null || username.equals("")) {
			user = DB.getInstance().getUserByPassword(password);
		} else {
			user = DB.getInstance().getUserByUsernameAndPassword(username, password);
		}
		return user;
	}

	public void removeUser(String username) {
		DB.getInstance().deleteUser(username);
	}

	public void updateUser(User u) {
		DB.getInstance().updateUser(u.getUsername(), u.getUsername(), u.getPassword(), u.isAdmin());
	}

	public List<User> getAllUsers(boolean includeAdmins) {
		return DB.getInstance().getAllUsers(includeAdmins);
	}

	public boolean checkMacAddress() {
		NetworkInterface network;
		StringBuilder sb = new StringBuilder();
		try {
			network = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
			byte[] mac = network.getHardwareAddress();

			
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			System.out.println("your MAC: " + sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} 
		if (DB.getInstance().isUserAuthentified(sb.toString())) {
			return true;
		}
		else {
			
			return false;
		}
	}
}