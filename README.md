There is a documentation file Caffe App Specification in the download section. 
I will try to keep it up-to-date while we are developing.
Anyone can download it and edit, just follow versioning rules that are in the comment. 
Everyone should read the first version of this document to get the "bigger picture"
and the idea in which way this application should be developed.

We should all consult on every idea and after agreeing on it, it should be written in this document to make it official. 